# Poll Erweiterung für TYPO3

Erstelle Umfragen und Abstimmungen in TYPO3. Mehrere Fragetypen und Antwortlayouts. Es ist auch eine PRO-Version verfügbar, die benutzerdefinierte Benutzerantworten und mehr hinzufügt. (Umfrage, Poll, Opinion, Vote, SimplePoll)


**Features:**

*   Anlegen beliebig vieler Umfragen mit beliebig vielen Fragen
*   Beliebig viele Antwortmöglichkeiten, bestehend aus Texten und Bildern
*   Plugin zum Anzeigen einer Umfrage inkl. Ausfüllmöglichkeit und optionalem Ergebnis am Ende
*   Plugin zum Anzeigen von Umfrage-Teasern (bspw. für die Startseite oder Sidebar)
*   Plugin zum Anzeigen aller Umfragen in einer Liste
*   Separates Plugin für das Ergebnis der Umfrage (direkte Auswahl einer Umfrage oder per Parameter möglich)
*   Einfache Umfragen (mehrere Antworten je Frage; eine oder mehrere Antworten)
*   Mehrfachteilnahmen können mittels Cookie unterbunden werden
*   Eingeloggte Benutzer können begonnene Umfragen zwischenspeichern
*   Lässt sich auch einfach in ein Intranet zur Befragung von Mitarbeitern integrieren

**Pro-Features:**

*   Backend-Modul, mit dem sich Umfragen und Umfrageergebnisse verwalten lassen
*   Größere Umfragen auf mehreren Seiten anzeigen (Divider 2 Tabs)
*   Ergebnisse einer Umfrage als CSV exportieren
*   Ergebnisse einer Umfrage automatisch via Scheduler-Mail verschicken
*   Benutzerdefinierte Antworten: Benutzer können auch eigene Antworten auf Fragen eingeben
*   Umfrage und zugehörige Daten zurücksetzen

**Mögliche Features:**

*   Statistik
*   Spamschutz mittels Captcha
*   Dashboard Widgets
*   Versenden der Ergebnisse per E-Mail

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Poll Dokumentation](https://www.coding.ms/documentation/typo3-poll "Poll Dokumentation")
*   [Poll Bug-Tracker](https://gitlab.com/codingms/typo3-public/poll/-/issues "Poll Bug-Tracker")
*   [Poll Repository](https://gitlab.com/codingms/typo3-public/poll "Poll Repository")
*   [TYPO3 Poll Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-poll/ "TYPO3 Poll Produktdetails")
*   [TYPO3 Poll Dokumentation](https://www.coding.ms/de/dokumentation/typo3-poll "TYPO3 Poll Dokumentation")
*   [TYPO3 Poll Download](https://extensions.typo3.org/extension/poll/ "TYPO3 Poll Download")
