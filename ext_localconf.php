<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// Plugins
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Poll',
    'Poll',
    [\CodingMs\Poll\Controller\PollController::class => 'show'],
    [\CodingMs\Poll\Controller\PollController::class => 'show'],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Poll',
    'PollTeaser',
    [\CodingMs\Poll\Controller\PollController::class => 'teaser'],
    [\CodingMs\Poll\Controller\PollController::class => 'teaser'],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Poll',
    'PollResult',
    [\CodingMs\Poll\Controller\PollController::class => 'result'],
    [\CodingMs\Poll\Controller\PollController::class => 'result'],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Poll',
    'PollList',
    [\CodingMs\Poll\Controller\PollController::class => 'list'],
    [\CodingMs\Poll\Controller\PollController::class => 'list'],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
