/**
 * Poll
 *
 * Thomas Deuling <typo3@Coding.ms>
 * 2017-01-14 - Muenster/Germany
 */
var Poll = {

	/**
	 * Initialize poll
	 */
	initialize: function() {
		// Deactivate form submit button on first click
		var saveButton = jQuery('.poll-button .poll-save');
		if(saveButton.length>0) {
			saveButton.click(function() {
				jQuery(this).attr('disabled', 'disabled');
				jQuery('<input />')
					.attr('type', 'hidden')
					.attr('name', jQuery(this).attr('name'))
					.attr('value', jQuery(this).attr('value'))
					.appendTo(jQuery(this).closest('form'));
				jQuery(this).closest('form').submit();
				return false;
			});
		}
		// Deactivate form submit button on first click
		var finishButton = jQuery('.poll-button .poll-finish');
		if(finishButton.length>0) {
			finishButton.click(function() {
				jQuery(this).attr('disabled', 'disabled');
				jQuery('<input />')
					.attr('type', 'hidden')
					.attr('name', jQuery(this).attr('name'))
					.attr('value', jQuery(this).attr('value'))
					.appendTo(jQuery(this).closest('form'));
				jQuery(this).closest('form').submit();
				return false;
			});
		}
		// Radiobuttons with user answers
		var radiobuttonWithUserAnswer = jQuery('.poll-radiobutton-user-answer input');
		if(radiobuttonWithUserAnswer.length>0) {
			jQuery.each(radiobuttonWithUserAnswer, function() {
				var userInput = jQuery(this);
				var pollAnswer = userInput.closest('.poll-answer');
				var pollQuestionAnswer = userInput.closest('.poll-question-answers');
				var radiobutton = jQuery('input[type=\'radio\']', pollAnswer);
				var radiobuttons = jQuery('input[type=\'radio\']', pollQuestionAnswer);
				if(!radiobutton.is(':checked')) {
					userInput.attr('disabled', 'disabled');
				}
				// Deactivate and clear user field on radiobutton
				radiobuttons.change(function() {
					var thisPollAnswer = jQuery(this).closest('.poll-answer');
					var allPollAnswer = jQuery('.poll-answer', jQuery(this).closest('.poll-question-answers'));
					jQuery.each(allPollAnswer, function() {
						var currentPollAnswer = jQuery(this);
						if(currentPollAnswer.attr('id') === thisPollAnswer.attr('id')) {
							jQuery('.poll-radiobutton-user-answer input', currentPollAnswer).removeAttr('disabled');
						}
						else {
							var userAnswer = jQuery('.poll-radiobutton-user-answer input', currentPollAnswer);
							if(userAnswer.length>0) {
								userAnswer.attr('disabled', 'disabled').val('');
							}
						}
					});
				});
			});
		}
		// Checkbox with user answers
		var checkboxWithUserAnswer = jQuery('.poll-checkbox-user-answer input');
		if(checkboxWithUserAnswer.length>0) {
			jQuery.each(checkboxWithUserAnswer, function() {
				var userInput = jQuery(this);
				var pollAnswer = userInput.closest('.poll-answer');
				var checkbox = jQuery('input[type=\'checkbox\']', pollAnswer);
				if(!checkbox.is(':checked')) {
					userInput.attr('disabled', 'disabled');
				}
				checkbox.change(function() {
					if(jQuery(this).is(':checked')) {
						jQuery('.poll-checkbox-user-answer input', pollAnswer).removeAttr('disabled');
					}
					else {
						jQuery('.poll-checkbox-user-answer input', pollAnswer)
							.attr('disabled', 'disabled')
							.val('');
					}
				});
			});
		}
		// Initialize poll tab next/previous
		jQuery('.tab-navigation .btn').click(function(event) {
			event.preventDefault();
			var button = jQuery(this);
			var tabNavigation = button.closest('.tab-navigation');
			var pollContainer = tabNavigation.closest('.tx-poll').parent();
			var parent = button.parent();
			button.removeClass('hide');
			if (parent.hasClass('previous')) {
				button.addClass('hide');
				tabNavigation.find('.active').parent().prev().find('.btn').click();
			}
			if (parent.hasClass('next')) {
				button.addClass('hide');
				tabNavigation.find('.active').parent().next().find('.btn').click();
			}
			var next = parent.next();
			var previous = parent.prev();
			if (next) {
				if (next.hasClass('next')) {
					next.find('.btn').addClass('hide');
				}
				if (next.hasClass('nav-item')) {
					tabNavigation.find('.next .btn').removeClass('hide');
				}
			}
			if (previous) {
				if (previous.hasClass('previous')) {
					previous.find('.btn').addClass('hide');
				}
				if (previous.hasClass('nav-item')) {
					tabNavigation.find('.previous .btn').removeClass('hide');
				}
			}
			window.scrollTo(pollContainer.offset().left, pollContainer.offset().top - 150);
		});
	}
};
jQuery(document).ready(function () {
	Poll.initialize();
});
