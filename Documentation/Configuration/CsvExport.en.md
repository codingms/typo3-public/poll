# Export survey results as CSV

## Manual CSV export

> #### Compatibility: {.alert .alert-warning}
>
> Requires Poll-Pro, version 1.0.0 or higher

To manually export your survey results as CSV, simply open the survey module and click the Export button on the right in the survey list.


> #### Warning: {.alert .alert-danger}
>
> Please note that if you change the answers later, there may be problems with the CSV export. It is therefore best to export your data before changing your questions and answers. After exporting your results, you're able to remove all participations by using the backend module, to restart with a clean base.



## Automated CSV export

> #### Compatibility: {.alert .alert-warning}
>
> Requires Poll-Pro, version 2.1.0 or higher

### Set up a task for automatic export

In order for the Poll extension to export surveys automatically, a scheduler task must be set up. To do this, proceed as follows:

1. Open the scheduler module in the TYPO3 backend and create a new task
2. Select the task _CSV Exporter_ from the poll_pro section
3. In the settings, enter one or more comma-separated email addresses as the target that should receive all survey exports
4. In the _Timeframe_ selection box, you can specify the period for which the survey data should be exported
5. Now enter a frequency for the execution and save the task

Your task is now ready to use.

### Mark surveys for export

Since it can happen that there are a lot of surveys in a system, the surveys to be exported must be marked. To do this, simply open a survey for editing and then tick the checkbox _Include results within scheduled CSV exports_.

When the scheduler and the associated task are executed, you will receive an email with CSV files for each marked survey attached.
