# Verfügbare Plugins

Hier findest Du alle Informationen über die verfügbaren Plugins dieser Extension.

## Umfrage anzeigen

Zeigt eine Umfrage an.

Dieses Plugin bietet die folgenden Einstellungen:

Name                  |Beschreibung
----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Umfrage anzeigen      |Hier kann ausgewählt werden, welche Umfrage angezeigt werden soll.

## Umfrage-Teaser anzeigen

Zeigt den Umfrage-Teaser der ausgewählten Umfrage an.

Dieses Plugin bietet die folgenden Einstellungen:

Name                       |Beschreibung
---------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Teaser-Überschrift         |Hier kann die Überschrift des Teasers festgelegt werden.
Seite für die Einzelansicht|Hier kann die Seite ausgewählt werden, auf der die Umfrage gemacht werden soll. Auf dieser Seite muss ein Plugin ``Umfrage anzeigen`` vorhanden sein.
Umfrage anzeigen           |Hier kann ausgewählt werden, welche Umfrage angezeigt werden soll.

## Umfrage-Ergebnis anzeigen

Zeigt das Ergebnis der Umfrage an. Dieses Plugin wird als Ziel-Seite nach dem Abschließen einer Umfrage platziert, so dass das Ergebnis auf einer separaten Seite angezeigt werden kann. Dieses Plugin bietet der Zeit noch keine Einstellungen.

## Umfrage-Liste anzeigen

Zeigt eine Liste aller Umfragen aus dem zugewiesenen Storage an.

Dieses Plugin bietet die folgenden Einstellungen:

Name                       |Beschreibung
---------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Listen-Überschrift         |Hier kann die Überschrift der Liste festgelegt werden.
Seite für die Einzelansicht|Hier kann die Seite ausgewählt werden, auf der die Umfrage gemacht werden soll. Auf dieser Seite muss ein Plugin ``Umfrage anzeigen`` vorhanden sein.
Offset der Liste           |Hier kann definiert werden, wie viele Einträge übersprungen werden sollen.
Feld für Sortierung        |Hier kann definiert werden, nach welchem Feld die Liste sortiert werden soll. Um die Einträge manuell zu sortieren, wähle "manuell" aus.
Richtung der Sortierung    |Hier kannst Du wählen, ob nach dem zuvor gewählten Feld "absteigend" oder "aufsteigend" sortiert werden soll.
