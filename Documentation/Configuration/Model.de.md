# Datensätze/Model

## Umfragetickets

Als "Umfragetickets" werden abgeschlossene Umfrage-Teilnahmen bezeichnet, die als eigener Datensatz
abgelegt werden.

In den Plugin-Einstellungen können "Umfragetickets" deaktiviert werden. Standardmäßig ist
diese Option aktiviert.

Wenn benutzerdefinierte Antworten (Freitext-Felder) verwendet werden sollen, muss diese Option aktiviert sein.

Wenn diese Option deaktiviert wird, werden die Antworten direkt in den Antwort-Datensätzen gespeichert.
Dies ermöglicht eine etwas höhere Performance. Zudem kann die Umfrage auch während sie bereits
läuft, noch modifiziert werden, ohne durch die Änderung der Datenstruktur die Exportfunktion unbrauchbar zu
machen.

Wenn diese Option aktiviert ist, können eingeloggte Frontend-Benutzer begonnene Umfragen zu einem
späteren Zeitpunkt weiterführen.

## Umfrage-Datensatz/Poll-Model

Dieser Datensatz enthält die Umfrage. Eine Umfrage kann mehrere Fragen enthalten.

Eine Umfrage hat die folgenden Einstellungen:

Name                       |Beschreibung
---------------------------|-------------------------------------------------------------------------------------------
Sprache                    |Hier wird die Sprache, in der diese Umfrage angelegt wurde, festgelegt.
Verbergen                  |Hier kann die Umfrage deaktiviert werden.
Title der Umfrage          |Hier kann der Titel der Umfrage eingegeben werden.
Beschreibung               |Hier kann ein einleitender Text zur Umfrage eingegeben werden.
Umfrage-Fragen             |Hier können eine oder mehrere Fragen für die Umfrage angelegt werden, diese sind vom Typ Umfrage-Frage/PollQuestion-Model


## Umfrage-Frage-Datensatz/PollQuestion-Model

Dieser Datensatz enthält die Fragen der Umfrage. Eine Frage kann von verschiedenen Frage-Typen sein.

Eine Frage hat die folgenden Einstellungen:

Name                        |Beschreibung
----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Frage                       |Hier wird die Umfrage-Frage eingetragen.
Position                    |Die Position gibt an, in welcher Reihenfolge die Fragen angezeigt werden (sofern man mehrere Fragen definiert hat). Die Reihenfolge kann einfach via Drag'n'Drop auf dem grauen Titel-Balken des Datensatzes angepasst werden.
Frage-Typ                   |Je nach Frage-Typ, hat die Frage ein unterschiedliches Antwort-Verhalten.
Antworten                   |Hier werden die möglichen Antworten der Frage eingetragen, diese sind vom Typ Umfrage-Frage-Antwort/PollQuestionAnswer-Model

### Fragetypen
Name                  |Beschreibung
----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Single                | Dieser Frage-Typ verwendet zum Antworten Radio-Buttons, somit ist nur eine Antwort möglich.
Multiple              | Dieser Frage-Typ verwendet zum Antworten Checkboxen, somit sind mehrere Antworten möglich.
SingleUserAnswer      | Dieser Frage-Typ verwendet zum Antworten ein Textfeld, somit ist eine frei eingebbare Antwort möglich.
MultipleUserAnswer    | Dieser Frage-Typ verwendet zum Antworten mehrere Textfelder, somit sind mehrere frei eingebbare Antworten möglich.

## Umfrage-Frage-Antwort-Datensatz/PollQuestionAnswer-Model

Dieser Datensatz enthält eine mögliche Antwort auf eine Fragen. Eine Frage kann mehrere Antworten haben.

Eine Antwort hat die folgenden Einstellungen:

Name                                        |Beschreibung
--------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Antwort                                     |Hier wird die Antwort eingetragen.
Position                                    |Die Position gibt an, in welcher Reihenfolge die Antworten angezeigt werden. Die Reihenfolge kann einfach via Drag'n'Drop auf dem grauen Titel-Balken des Datensatzes angepasst werden.
Teilnehmer kann selbst eine Antwort eingeben|Hier kann gewählt werden, ob der Benutzer auch selbst eine Antwort eingeben darf.
Platzhalter für Antwort-Textfeld            |Hier kann der Platzhalter für das Textfeld der frei eingebbaren Antwort angegeben werden.

## Umfragezicket-Datensatz/PollTicket-Model

Dieser Datensatz enthält die gespeicherten Antworten auf eine Umfrage. Für jeden Teilnehmer wird
dabei ein Umfrageticket erstellt.

**Hinweis:** Ein Umfrageticket kann nicht gelöscht werden.

Ein Umfrageticket hat die folgenden Einstellungen:

NName                                       |Beschreibung
--------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Antwort                                     |Hier wird die Antwort eingetragen.
Position                                    |Die Position gibt an, in welcher Reihenfolge die Antworten angezeigt werden. Die Reihenfolge kann einfach via Drag'n'Drop auf dem grauen Titel-Balken des Datensatzes angepasst werden.
Teilnehmer kann selbst eine Antwort eingeben|Hier kann gewählt werden, ob der Benutzer auch selbst eine Antwort eingeben darf.
Platzhalter für Antwort-Textfeld            |Hier kann der Platzhalter für das Textfeld der frei eingebbaren Antwort angegeben werden.
