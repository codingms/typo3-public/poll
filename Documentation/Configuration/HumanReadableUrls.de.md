# Sprechende URLs

## Slug Konfiguration (ab TYPO3 9.5)

```yaml
routeEnhancers:
  PageTypeSuffix:
    type: PageType
  PollPlugin:
    type: Extbase
    extension: Poll
    plugin: Poll
    routes:
      - routePath: '/{poll_slug}'
        _controller: 'Poll::show'
        _arguments:
          poll_slug: poll
    defaultController: 'Poll::list'
    aspects:
      poll_slug:
        type: PersistedAliasMapper
        tableName: tx_poll_domain_model_poll
        routeFieldName: slug
        routeValuePrefix: /
  PollresultPlugin:
    type: Extbase
    extension: Poll
    plugin: Pollresult
    routes:
      - routePath: '/{poll_result}/result'
        _controller: 'Poll::result'
        _arguments:
          poll_result: poll
    defaultController: 'Poll::list'
    aspects:
      poll_result:
        type: PersistedAliasMapper
        tableName: tx_poll_domain_model_poll
        routeFieldName: slug
        routeValuePrefix: /
```


## Realurl Konfiguration

```php
$configuration = [
	'postVarSets' => [
		'_DEFAULT' => [

			// EXT:poll start
			'umfrage' => [
				[
					'GETvar' => 'tx_poll_poll[controller]',
					'valueMap' => [
						'daten' => 'Poll',
					],
					'noMatch' => 'bypass',
				],
				[
					'GETvar' => 'tx_poll_poll[action]',
					'valueMap' => [
						'anzeigen' => 'showPoll',
						'abmelden' => 'unsubscribe',
						'anmeldung-bestaetigen' => 'subscribeConfirm',
						'abmeldung-bestaetigen' => 'unsubscribeConfirm',
					],
					'noMatch' => 'bypass',
				],
				[
					'GETvar' => 'tx_poll_poll[poll]',
					'lookUpTable' => [
					'table' => 'tx_poll_domain_model_poll',
					'id_field' => 'uid',
					'alias_field' => 'title',
					'addWhereClause' => ' AND NOT deleted',
					'useUniqueCache' => 1,
					'useUniqueCache_conf' => [
						'strtolower' => 1,
						'spaceCharacter' => '-',
						],
					],
				],
			],
			// EXT:poll end

		],
	],
];
```
