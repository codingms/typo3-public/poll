# TypoScript-Konstanten Einstellungen


## Poll Container

| **Konstante**    | themes.configuration.container.poll                       |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Container für Umfrage-Datensätze                          |
| **Beschreibung** |                                                           |
| **Typ**          | int+                                                      |
| **Standardwert** | 0                                                         |



## Umfrageseiten

| **Konstante**    | themes.configuration.pages.login                          |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Login                                                     |
| **Beschreibung** |                                                           |
| **Typ**          | int+                                                      |
| **Standardwert** | 0                                                         |

| **Konstante**    | themes.configuration.pages.poll.list                      |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Seite für Umfrageliste                                    |
| **Beschreibung** |                                                           |
| **Typ**          | int+                                                      |
| **Standardwert** | 0                                                         |

| **Konstante**    | themes.configuration.pages.poll.detail                    |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Seite für Umfragedetails                                  |
| **Beschreibung** |                                                           |
| **Typ**          | int+                                                      |
| **Standardwert** | 0                                                         |

| **Konstante**    | themes.configuration.pages.poll.notFound                  |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Redirectseite für Umfrage nicht gefunden                  |
| **Beschreibung** |                                                           |
| **Typ**          | int+                                                      |
| **Standardwert** | 0                                                         |

| **Konstante**    | themes.configuration.pages.poll.result                    |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Seite für Umfrageergebnis                                 |
| **Beschreibung** |                                                           |
| **Typ**          | int+                                                      |
| **Standardwert** | 0                                                         |



## Umfrage Vorlagen

| **Konstante**    | themes.configuration.extension.poll.view.templateRootPath |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Template root path                                        |
| **Beschreibung** |                                                           |
| **Typ**          | string                                                    |
| **Standardwert** | EXT:poll/Resources/Private/Templates/                     |

| **Konstante**    | themes.configuration.extension.poll.view.partialRootPath  |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Partial root path                                         |
| **Beschreibung** |                                                           |
| **Typ**          | string                                                    |
| **Standardwert** | EXT:poll/Resources/Private/Partials/                      |

| **Konstante**    | themes.configuration.extension.poll.view.layoutRootPath   |
|:-----------------|:----------------------------------------------------------|
| **Label**        | Layout root path                                          |
| **Beschreibung** |                                                           |
| **Typ**          | string                                                    |
| **Standardwert** | EXT:poll/Resources/Private/Layouts/                       |



