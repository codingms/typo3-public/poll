# TypoScript constant settings


## Poll Container

| **Constant**      | themes.configuration.container.poll                       |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Container for poll records                                |
| **Description**   |                                                           |
| **Type**          | int+                                                      |
| **Default value** | 0                                                         |



## Poll Pages

| **Constant**      | themes.configuration.pages.login                          |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Login                                                     |
| **Description**   |                                                           |
| **Type**          | int+                                                      |
| **Default value** | 0                                                         |

| **Constant**      | themes.configuration.pages.poll.list                      |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Page for poll list                                        |
| **Description**   |                                                           |
| **Type**          | int+                                                      |
| **Default value** | 0                                                         |

| **Constant**      | themes.configuration.pages.poll.detail                    |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Page for poll details                                     |
| **Description**   |                                                           |
| **Type**          | int+                                                      |
| **Default value** | 0                                                         |

| **Constant**      | themes.configuration.pages.poll.notFound                  |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Redirect page for poll not found                          |
| **Description**   |                                                           |
| **Type**          | int+                                                      |
| **Default value** | 0                                                         |

| **Constant**      | themes.configuration.pages.poll.result                    |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Page for poll result                                      |
| **Description**   |                                                           |
| **Type**          | int+                                                      |
| **Default value** | 0                                                         |



## Poll Templates

| **Constant**      | themes.configuration.extension.poll.view.templateRootPath |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Template root path                                        |
| **Description**   |                                                           |
| **Type**          | string                                                    |
| **Default value** | EXT:poll/Resources/Private/Templates/                     |

| **Constant**      | themes.configuration.extension.poll.view.partialRootPath  |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Partial root path                                         |
| **Description**   |                                                           |
| **Type**          | string                                                    |
| **Default value** | EXT:poll/Resources/Private/Partials/                      |

| **Constant**      | themes.configuration.extension.poll.view.layoutRootPath   |
| :---------------- | :-------------------------------------------------------- |
| **Label**         | Layout root path                                          |
| **Description**   |                                                           |
| **Type**          | string                                                    |
| **Default value** | EXT:poll/Resources/Private/Layouts/                       |



