# Installation

To install the poll extension:

* Install the extension using the Extension Manager or with composer (`composer req codingms / poll`).

* Open your main template and add a `Poll` entry under **Includes → Include static**.

* Create a **'Poll' container** in your TYPO3 page tree. This is the directory where you will create your polls and surveys. You can use the "List" module to create new **poll data records.** Try creating a poll now to try it out.

* The extension will need to know about your "container page". Switch to the **constant editor** and enter the ID of the container page under "Poll" -> "Container for poll records".

* Now **create a page** on your website for your survey. Add a `View Poll` plugin to the page and select the poll you just created. If you view the page the poll should now be displayed.
  If you activate "Show poll result in poll" in the plugin settings, the user will see the poll result before they take part in the survey.

* Now define a **final page** for the survey. This is the page that the visitor is redirected to once they have successfully completed the survey. Simply create a
  subpage of your current survey page. Add a `Show survey result` plugin to this page and enter the ID of this page in the constant editor as before
  under "Poll result page".

* You could also add a `Show Survey Teaser` plugin to your homepage (or other pages) to attract attention to your survey or add a `Show survey list` plugin so that your visitors can take part in older surveys.


## Installation of PRO version

The PRO version requires the **"modules" extension** to be installed, which you can find in the [TER](https://extensions.typo3.org/extension/modules "TER") or in our
[Git-Repository](https://gitlab.com/codingms/typo3-public/modules/-/tags "Git Repository Modules").
For TYPO3 8 please use version 3.2, for TYPO3 9 version 4.0.

Just like the basic version, install the PRO version using Extension Manager or composer
install (`composer req codingms / poll-pro`).
