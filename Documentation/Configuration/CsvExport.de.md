# Umfrage-Ergebnisse als CSV exportieren

## Manueller CSV-Export

> #### Kompatibilität: {.alert .alert-warning}
>
> Erfordert Poll-Pro, ab Version 1.0.0

Um deine Umfrage-Ergebnisse manuell als CSV zu exportieren, öffne einfach das Umfrage-Modul und klicke in der Umfrage-Liste rechts auf den Exportieren-Button.


> #### Achtung: {.alert .alert-danger}
>
> Bitte beachte, wenn Du die Antworten später veränderst kann es zu Problemen mit dem CSV-Export kommen. Exportiere daher am besten vor dem Verändern deiner Fragen und Antworten deine Daten. Nachdem Sie Ihre Ergebnisse exportiert haben, können Sie mithilfe des Backend-Moduls alle Teilnahmen entfernen, um mit einer sauberen Basis neu zu starten.



## Automatisierter CSV-Export

> #### Kompatibilität: {.alert .alert-warning}
>
> Erfordert Poll-Pro, ab Version 2.1.0

### Task für automatischen Export einrichten

Damit die Poll-Erweiterung Umfragen automatisch exportieren kann, muss ein Scheduler-Task eingerichtet werden. Dafür gehe wie folgt vor:

1. Öffen das Scheduler-Modul im TYPO3-Backend und erstelle einen neuen Task
2. Wähle den Task _CSV Exporter_ aus dem poll_pro Abschnitt
3. Gebe in den Settings eine oder mehrere Komma getrennte Mailadressen als Ziel an, welche alle Umfrage-Exporte erhalten sollen
4. In der _Timeframe_ Auswalbox kannst Du angeben, für welchen Zeitraum die Umfrage-Daten jeweils exportiert werden sollen
5. Gib jetzt noch eine Frequenz für die Ausführung an und speichere den Task

Dein Task ist nun einsatzbereit.

### Umfragen für Export markieren

Da es nun aber vorkommen kann, dass sehr viele Umfragen in einem System vorhanden sind, müssen die zu exportierenden Umfragen markiert werden. Dafür öffne einfach eine Umfrage zur Bearbeitung und setzen dann die Checkbox _Ergebnisse in regelmässigen CSV-Exporten berücksichtigen_.

Wenn nun der Scheduler und damit der verbundene Task ausgeführt wird, erhältst Du eine Mail mit CSV-Dateien zu jeder markierten Umfrage im Anhang.
