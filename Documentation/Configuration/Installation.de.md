# Installation

Zur Installation der Umfrage-Extension führst Du die folgenden Schritte durch:

* Installiere die Extension über den Extension-Manager oder
  mittels composer (`composer req codingms/poll`).

* Öffne Dein Haupt-Template und füge unter **Includes → Include static** den Eintrag `Poll` hinzu.

* Erstelle in Deinem TYPO3-Seitenbaum einen **Container**  `Umfragen`. In diesem Verzeichnis
  lege Deine Umfragen an. Dazu verwendest Du das "Liste"-Modul und erstellst einen neuen Datensatz
  vom Typ "Umfrage". Lege am besten nun eine Umfrage an.

* Diese "Container-Seite" muss der Erweiterung bekannt gemacht werden. Wechsel dazu in den
  **Konstanten-Editor** und trage die ID der Seite unter "Poll" --> "Container für Umfrage-Datensätze" ein.

* Erstelle nun auf Deiner Webseite **im Seitenbaum eine Seite**, auf der Du die Umfrage durchführen willst.
  Füge auf der Seite ein **Plugin** vom Typ `Umfrage anzeigen` ein und wähle die zuvor angelegte
  Umfrage aus. Wenn Du Dir nun die Seite ansiehst, sollte die Umfrage bereits angezeigt werden.
  Wenn Du die Option "Ergebnis der Umfrage unter der Umfrage anzeigen" in den Plugin-Einstellungen
  aktivierst, wird das Umfrageergebnis bereits angezeigt, bevor der Nutzer selber an der
  Umfrage teilgenommen hat.

* Nun müssen wir noch eine **Abschlussseite** für die Umfrage definieren. Dies ist die Seite, auf die der Besucher
  umgeleitet wird, wenn er erfolgreich an der Umfrage teilgenommen hat. Dazu erstellst Du eine
  Unterseite der aktuellen Umfrage-Seite. Auf dieser Seite platzierst Du ein **Plugin**
  vom Typ `Umfrage-Ergebnis anzeigen`. Die ID dieser Seite trägst Du wieder im **Konstanten-Editor**
  unter "Seite für Umfrageergebnis" ein.

* Optional kannst Du nun noch auf Deiner **Startseite** o.ä. eine **Plugin** vom Typ
  `Umfrage-Teaser anzeigen` einfügen, um Besucher auf Deine Umfrage aufmerksam
  zu machen, oder ein Plugin vom Typ `Umfrage-Liste anzeigen` platzieren,
  damit Deine Besucher auch an älteren Umfragen teilnehmen können.


## Installation der PRO-Version

Für die Pro-Version benötigst Du die **Extension "modules",** die Du im [TER](https://extensions.typo3.org/extension/modules "TER") unter
 oder in unserem [Git-Repository](https://gitlab.com/codingms/typo3-public/modules/-/tags "Git Repository") finden kannst.
Für TYPO3 8 verwendest Du bitte die Version 3.2, ab TYPO3 9 die Version 4.0.

Die PRO-Version kannst Du, wie die Basis-Version über den Extension-Manager oder mittels composer
installieren (`composer req codingms/poll-pro`).
