# Poll Migration

## Version 4.0.0

* If you've overridden the fluid templates, you might need to refactor the `property` argument in the `f:form` - see original templates shipped in this extension.



## Version 3.1.0

* If you like to use the dividers-to-tabs, you might need update your Show.html Template (if overridden)
