# Poll Extension for TYPO3

Create polls/surveys/Umfragen in TYPO3. Multiple question types and answer layouts. There is also a PRO version available which adds custom user answers and more. (Umfrage, Poll, Opinion, Vote, SimplePoll)


**Features:**

*   Create unlimited numbers of polls with any number of questions
*   Many different answer options consisting of text and images
*   Plugin for displaying polls including data entry options and optional display of results on completion
*   Plugin for displaying poll teasers (e.g. on your home page or sidebar)
*   Plugin for displaying lists of all polls
*   Separate plugin to display poll results (selection by plugin or per parameter possible
*   Simple polls/surveys (multiple answers per question; one or more answers)
*   Prevent repeat participation by setting cookie
*   Logged in frontend users are able to save started polls
*   Can also be easily integrated into an intranet for surveying employees

**Pro-Features:**

*   Backend module for managing polls and poll results
*   Display larger polls on multiple pages (divider 2 tabs)
*   Export poll results as CSV file
*   Send poll results automatically via scheduler-mail
*   User-defined answers: users can enter their own answers to questions
*   Reset polls and related data

**Possible Features:**

*   Statistics
*   Spam protection using Captcha
*   Dashboard widgets
*   Send results by email

If you need some additional or custom feature - get in contact!


**Links:**

*   [Poll Documentation](https://www.coding.ms/documentation/typo3-poll "Poll Documentation")
*   [Poll Bug-Tracker](https://gitlab.com/codingms/typo3-public/poll/-/issues "Poll Bug-Tracker")
*   [Poll Repository](https://gitlab.com/codingms/typo3-public/poll "Poll Repository")
*   [TYPO3 Poll Productdetails](https://www.coding.ms/typo3-extensions/typo3-poll/ "TYPO3 Poll Productdetails")
*   [TYPO3 Poll Documentation](https://www.coding.ms/documentation/typo3-poll "TYPO3 Poll Documentation")
*   [TYPO3 Poll Download](https://extensions.typo3.org/extension/poll/ "TYPO3 Poll Download")
