<?php

namespace CodingMs\Poll\Service\Validate;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\PollQuestion;
use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Validation for questions
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class ValidateMultiple extends AbstractValidate
{
    /**
     * Validates a question
     *
     * @param array<string, string> $answersIn Incoming answer data
     * @param array<string, string> $answersOut Outgoing answer data
     * @param PollQuestion $question Poll question with
     * @param array<int, string> $messages Array with error messages
     */
    public function validate($answersIn, &$answersOut, $question, &$messages)
    {
        //
        // See AbstractValidate for detailed method description!!!
        //
        $qid = $question->getUid();
        $qidKey = 'question_' . $qid;
        // For each question answer
        $questionAnswers = $question->getQuestionAnswer();
        if (!empty($questionAnswers)) {
            $answered = false;
            /** @var PollQuestionAnswer $questionAnswer */
            foreach ($questionAnswers as $questionAnswer) {
                $qaid = $questionAnswer->getUid();
                $qaidKey = $qidKey . '_' . $qaid;
                if (isset($answersIn[$qaidKey]) && trim($answersIn[$qaidKey]) !== '') {
                    $answersOut[$qaidKey] = $answersIn[$qaidKey];
                    $answered = true;
                }
            }
            if (!$answered && !$question->getOptional()) {
                $messages[$qid] = LocalizationUtility::translate(
                    'tx_poll_message.error_validate_multiple_required_fails',
                    'Poll',
                    [$question->getQuestion()]
                );
            }
        }
    }
}
