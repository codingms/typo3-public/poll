<?php

namespace CodingMs\Poll\Service\Validate;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\PollQuestion;

/**
 * Validation for questions
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
abstract class AbstractValidate
{
    /**
     * @param array<string, string> $answersIn Incoming answer data
     * @param array<string, string> $answersOut Outgoing answer data
     * @param PollQuestion $question Poll question with
     * @param array<int, string> $messages Array with error messages
     */
    public function validate($answersIn, &$answersOut, $question, &$messages)
    {
        //
        // $answerIn is an array with incoming answer data.
        // Radiobutton (optionally with user answer):
        // array(2) {
        //   ["question_32"]=> string(3) "170"
        //   ["question_32_userAnswer"]=> string(4) "test"
        // }
        // In this example the 32 is the question uid.
        // In this example the 170 is the question answer uid.
        //
        // Checkbox (optionally with user answer; one key for each selection)
        // array(3) {
        //   ["question_37_186"]=> string(0) ""
        //   ["question_37_187"]=> string(3) "187"
        //   ["question_37_188"]=> string(3) "188"
        //   ["question_37_188_userAnswer"]=> string(3) "test"
        // }
        // If a checkbox is selected, the value is represented by the answer uid!
        // In this example the 37 is the question uid.
        // In this example the 187 is the question answer uid.
        // The selected 188 checkbox has an user answer (filled with test).
        //
        //
        // $answerOut is an array with outgoing answer data.
        // Validated answer data is passed with equal array-keys to this $answerOut array.
        //
        //
        // $question is the PollQuestion record, where the answers are contained.
        //
        //
        // $messages is an array of error messages - each question one key.
        // $messages[32] => contains the error message for question with uid 32
    }
}
