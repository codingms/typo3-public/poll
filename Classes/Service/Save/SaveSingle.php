<?php

namespace CodingMs\Poll\Service\Save;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\PollQuestion;
use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use CodingMs\Poll\Domain\Model\PollTicket;
use CodingMs\Poll\Domain\Model\PollTicketAnswer;
use Exception;

/**
 * Saving for questions
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class SaveSingle extends AbstractSave
{
    /**
     * Saves a question
     *
     * @param PollTicket $pollTicket
     * @param array<string, string> $answersArray
     * @param PollQuestion $question
     * @return PollTicket Updated poll ticket
     * @throws Exception
     */
    public function save(PollTicket $pollTicket, array $answersArray, PollQuestion $question)
    {
        $qid = $question->getUid();
        $this->answerKey = 'question_' . $qid;
        $this->getPollQuestionAnswer($answersArray);
        //
        // Update an existing answer
        if ($this->getPollTicketAnswer($pollTicket, $question) &&
            $this->pollTicketAnswer instanceof PollTicketAnswer &&
            $this->pollQuestionAnswer instanceof PollQuestionAnswer) {
            $this->pollTicketAnswer->setPollQuestionAnswer($this->pollQuestionAnswer);
        } else {
            //
            // Insert a new answer
            $pollTicket = $this->insert($pollTicket, $answersArray, $question);
        }
        return $pollTicket;
    }
}
