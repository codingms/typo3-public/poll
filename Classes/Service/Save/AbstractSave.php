<?php

namespace CodingMs\Poll\Service\Save;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Poll\Domain\Model\PollQuestion;
use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use CodingMs\Poll\Domain\Model\PollTicket;
use CodingMs\Poll\Domain\Model\PollTicketAnswer;
use CodingMs\Poll\Domain\Repository\PollQuestionAnswerRepository;
use Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Saving for questions
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
abstract class AbstractSave
{
    /**
     * @var string
     */
    protected $answerKey = '';

    /**
     * poll question answer (original from poll!)
     *
     * @var PollQuestionAnswer|null
     */
    protected $pollQuestionAnswer;

    /**
     * ticket answer
     *
     * @var PollTicketAnswer|null
     */
    protected $pollTicketAnswer;

    /**
     * @var PollQuestionAnswerRepository
     */
    protected $pollQuestionAnswerRepository;

    public function __construct()
    {
        $this->pollQuestionAnswerRepository = GeneralUtility::makeInstance(PollQuestionAnswerRepository::class);
    }

    /**
     * Reset this service for a new execution
     */
    public function reset()
    {
        $this->answerKey = '';
        $this->pollQuestionAnswer = null;
        $this->pollTicketAnswer = null;
    }

    /**
     * Saves a question
     *
     * @param PollTicket $pollTicket
     * @param array<string, string> $answersArray
     * @param PollQuestion $question
     * @return PollTicket Updated poll ticket
     */
    abstract public function save(PollTicket $pollTicket, array $answersArray, PollQuestion $question);

    /**
     * Identify the ticket answer by given question.
     * The ticket answer saved in pollTicketAnswer property.
     *
     * @param PollTicket $pollTicket
     * @param PollQuestion $question
     * @return bool True if ticket answer found
     */
    protected function getPollTicketAnswer(PollTicket $pollTicket, PollQuestion $question)
    {
        $alreadyAnswered = false;
        $pollTicketAnswers = $pollTicket->getPollTicketAnswer();
        if ($pollTicketAnswers->count()) {
            /** @var PollTicketAnswer $pollTicketAnswer */
            foreach ($pollTicketAnswers as $pollTicketAnswer) {
                if ($pollTicketAnswer->getPollQuestion()->getUid() === $question->getUid()) {
                    // Remind the answer object
                    $this->pollTicketAnswer = $pollTicketAnswer;
                    $alreadyAnswered = true;
                    break;
                }
            }
        }
        return $alreadyAnswered;
    }

    /**
     * Identify the question answer from the poll (option for radiobutton or checkbox).
     * The question answer saved in pollQuestionAnswer property.
     *
     * @param array<string, string> $answersArray
     * @throws Exception
     */
    protected function getPollQuestionAnswer(array $answersArray)
    {
        // if the answer is available..
        if (isset($answersArray[$this->answerKey])) {
            // get the answer object
            $pollQuestionAnswer = $this->pollQuestionAnswerRepository->findByUid((int)$answersArray[$this->answerKey]);
            if ($pollQuestionAnswer instanceof PollQuestionAnswer) {
                $this->pollQuestionAnswer = $pollQuestionAnswer;
            } else {
                throw new Exception('PollQuestionAnswer not found (uid:' . $answersArray[$this->answerKey] . ', answerKey:' . $this->answerKey . ')');
            }
        }
    }

    /**
     * Save the answer in poll ticket.
     *
     * @param PollTicket $pollTicket
     * @param array<string, mixed> $answer
     * @param PollQuestion $question
     * @return PollTicket
     */
    protected function insert(PollTicket $pollTicket, array $answer, PollQuestion $question)
    {
        $qid = $question->getUid();
        $qidKey = 'question_' . $qid;
        $qidKeyUser = 'question_' . $qid . '_userAnswer';
        // Radiobutton answer
        if (isset($answer[$qidKey])) {
            /** @var PollTicketAnswer $pollTicketAnswer */
            $pollTicketAnswer = GeneralUtility::makeInstance(PollTicketAnswer::class);
            // Insert ticket answer
            $pollTicketAnswer->setPollQuestion($question);
            // User answer available?
            // Single user answer
            if (isset($answer[$qidKeyUser])) {
                $pollTicketAnswer->setUserAnswer($answer[$qidKeyUser]);
            } else {
                // Radio button user answer
                /** @var PollQuestionAnswer $questionAnswer */
                foreach ($question->getQuestionAnswer() as $questionAnswer) {
                    $qaid = $questionAnswer->getUid();
                    $qaidKey = $qidKey . '_' . $qaid;
                    $qaidKeyUser = $qaidKey . '_userAnswer';
                    if (isset($answer[$qaidKeyUser])) {
                        $pollTicketAnswer->setUserAnswer($answer[$qaidKeyUser]);
                    }
                }
            }
            /** @var PollQuestionAnswer $pollQuestionAnswer */
            $pollQuestionAnswer = $this->pollQuestionAnswerRepository->findByIdentifier((int)$answer[$qidKey]);
            if ($pollQuestionAnswer instanceof PollQuestionAnswer) {
                $pollTicketAnswer->setPollQuestionAnswer($pollQuestionAnswer);
                $pollTicket->addPollTicketAnswer($pollTicketAnswer);
            }
        }
        // Checkbox answer
        /** @var PollQuestionAnswer $questionAnswer */
        foreach ($question->getQuestionAnswer() as $questionAnswer) {
            $qaid = $questionAnswer->getUid();
            $qaidKey = $qidKey . '_' . $qaid;
            $qaidKeyUser = $qaidKey . '_userAnswer';
            // Checkbox answer
            if (isset($answer[$qaidKey]) && trim($answer[$qaidKey]) !== '') {
                /** @var PollTicketAnswer $pollTicketAnswer */
                $pollTicketAnswer = GeneralUtility::makeInstance(PollTicketAnswer::class);
                // Insert ticket answer
                $pollTicketAnswer->setPollQuestion($question);
                if (isset($answer[$qaidKeyUser])) {
                    $pollTicketAnswer->setUserAnswer($answer[$qaidKeyUser]);
                }
                /** @var PollQuestionAnswer $pollQuestionAnswer */
                $pollQuestionAnswer = $this->pollQuestionAnswerRepository->findByIdentifier((int)$answer[$qaidKey]);
                if ($pollQuestionAnswer instanceof PollQuestionAnswer) {
                    $pollTicketAnswer->setPollQuestionAnswer($pollQuestionAnswer);
                    $pollTicket->addPollTicketAnswer($pollTicketAnswer);
                }
            }
        }
        return $pollTicket;
    }
}
