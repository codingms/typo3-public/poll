<?php

namespace CodingMs\Poll\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Domain\Model\FrontendUser;
use CodingMs\Modules\Domain\Repository\FrontendUserRepository;
use CodingMs\Poll\Domain\Model\Poll;
use CodingMs\Poll\Domain\Model\PollQuestion;
use CodingMs\Poll\Domain\Model\PollQuestionAnswer;
use CodingMs\Poll\Domain\Model\PollTicket;
use CodingMs\Poll\Domain\Model\PollTicketAnswer;
use CodingMs\Poll\Domain\Repository\PollRepository;
use CodingMs\Poll\Domain\Repository\PollTicketRepository;
use Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;

/**
 * Poll utilities
 */
class PollUtility
{
    /**
     * @var PollRepository
     */
    protected $pollRepository;

    /**
     * @param PollRepository $pollRepository
     */
    public function injectPollRepository(PollRepository $pollRepository)
    {
        $this->pollRepository = $pollRepository;
    }

    /**
     * @var PollTicketRepository
     */
    protected $pollTicketRepository;

    /**
     * @param PollTicketRepository $pollTicketRepository
     */
    public function injectPollTicketRepository(PollTicketRepository $pollTicketRepository)
    {
        $this->pollTicketRepository = $pollTicketRepository;
    }

    /**
     * @var FrontendUserRepository
     */
    protected $frontendUserRepository;

    /**
     * @param FrontendUserRepository $frontendUserRepository
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    /**
     * Returns if a FrontendUser has already done a poll
     *
     * @param Poll $poll Poll-Object
     * @param FrontendUser $frontendUser FrontendUser-Object
     * @return bool
     */
    public function isPollAlreadyDoneByFrontendUser(Poll $poll, FrontendUser $frontendUser)
    {
        $pollTicket = $this->pollTicketRepository->findOneByPollAndFrontendUser($poll, $frontendUser);
        return $pollTicket instanceof PollTicket;
    }

    /**
     * Gets the current frontend user
     *
     * @return mixed
     * @throws Exception
     */
    public function getFrontendUser()
    {
        // User is logged in?
        if (isset($GLOBALS['TSFE']->fe_user->user)) {
            $frontendUserUid = (int)$GLOBALS['TSFE']->fe_user->user['uid'];
            if ($frontendUserUid > 0) {
                $frontendUser = $this->getUserByUidWithoutPid($frontendUserUid);
                if ($frontendUser instanceof FrontendUser) {
                    return $frontendUser;
                }
            }
        }
        return null;
    }

    /**
     * @param int $uid
     * @return mixed
     * @throws \Doctrine\DBAL\Exception
     */
    private function getUserByUidWithoutPid(int $uid)
    {
        /** @var DataMapper $dataMapper */
        $dataMapper = GeneralUtility::makeInstance(DataMapper::class);
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder*/
        $queryBuilder = $connectionPool->getQueryBuilderForTable('fe_users');
        $queryBuilder->select('*')
            ->from('fe_users')
            ->where($queryBuilder->expr()->eq('uid', $uid));
        return $dataMapper->map(FrontendUser::class, [$queryBuilder->executeQuery()->fetchAssociative()])[0];
    }

    /**
     * @param Poll $poll Poll-Object
     * @param FrontendUser|null $frontendUser FrontendUser-Object
     * @return bool
     * @throws Exception
     */
    public function getAlreadyParticipated(Poll $poll, FrontendUser $frontendUser = null)
    {
        $alreadyParticipated = false;
        //
        // Cookie protected and already participated
        $cookieKey = 'pollParticipate_' . $poll->getUid();
        if ($poll->getUseCookieProtection() && isset($_COOKIE[$cookieKey]) && $_COOKIE[$cookieKey]) {
            $alreadyParticipated = true;
        }
        //
        // Poll runs in frontendUser-mode..
        if ($poll->getMode() === 'frontendUser') {
            if ($frontendUser === null) {
                throw new Exception('FrontendUser required');
            }
            //
            // Poll isn't editable and already done by this frontend user
            $alreadyDone = $this->isPollAlreadyDoneByFrontendUser($poll, $frontendUser);
            if (!$poll->isEditable() && $alreadyDone) {
                $alreadyParticipated = true;
            }
        }
        return $alreadyParticipated;
    }

    /**
     * Restores a saved poll ticket
     *
     * @param Poll $poll
     * @param FrontendUser|null $frontendUser
     * @return array<string, mixed>
     */
    public function getPollTicketAnswersArray(Poll $poll, FrontendUser $frontendUser = null)
    {
        $answersArray = [];

        if ($frontendUser instanceof FrontendUser) {
            //
            // Only get the poll ticket, if there is already one
            /** @var PollTicket $pollTicket */
            $pollTicket = $this->pollTicketRepository->findOneByPollAndFrontendUser($poll, $frontendUser);
            if ($pollTicket instanceof PollTicket) {
                //
                // Don't restore in case of finished tickets
                if (!$pollTicket->isFinished()) {
                    $pollTicketAnswers = $pollTicket->getPollTicketAnswer();
                    if ($pollTicketAnswers->count() > 0) {
                        /** @var PollTicketAnswer $pollTicketAnswer */
                        foreach ($pollTicketAnswers as $pollTicketAnswer) {
                            $question = $pollTicketAnswer->getPollQuestion();
                            $questionKey = 'question_' . $question->getUid();
                            $questionAnswer = $pollTicketAnswer->getPollQuestionAnswer();
                            $qaid = $questionAnswer->getUid();
                            $questionAnswerKey = $questionKey . '_' . $qaid;
                            switch ($question->getQuestionType()) {
                                case 'Single':
                                    $answersArray[$questionKey] = $qaid;
                                    break;
                                case 'SingleWithOptionalUserAnswer':
                                    $answersArray[$questionKey] = $qaid;
                                    $answersArray[$questionAnswerKey . '_userAnswer'] = $pollTicketAnswer->getUserAnswer();
                                    break;
                                case 'Multiple':
                                    $answersArray[$questionAnswerKey] = $qaid;
                                    break;
                                case 'MultipleWithOptionalUserAnswer':
                                    $answersArray[$questionAnswerKey] = $qaid;
                                    $answersArray[$questionAnswerKey . '_userAnswer'] = $pollTicketAnswer->getUserAnswer();
                                    break;
                                case 'SingleUserAnswer':
                                    $answersArray[$questionKey] = $qaid;
                                    $answersArray[$questionKey . '_userAnswer'] = $pollTicketAnswer->getUserAnswer();
                                    break;
                            }
                        }
                        //
                    }
                }
            }
        }

        return $answersArray;
    }

    /**
     * Returns only a poll question answer uid
     * @param PollQuestion $pollQuestion
     * @param string $value
     * @return int
     */
    public function getPollQuestionAnswerUid(PollQuestion $pollQuestion, string $value): int
    {
        $uid = 0;
        $answers = $pollQuestion->getQuestionAnswer();
        if ($answers->count() > 0) {
            /** @var PollQuestionAnswer $answer */
            foreach ($answers as $answer) {
                if ($answer->getAnswer() === $value) {
                    $uid = (int)$answer->getUid();
                    break;
                }
            }
        }
        return $uid;
    }
}
