<?php

namespace CodingMs\Poll\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Tools
 *
 * @version 1.2.0
 */
class ToolsUtility
{
    /**
     * Gets a template path from configuration[view]
     * @param array<string, mixed> $configurationView Pass $configuration['view'] in this
     * @param string $type Type of template path (for example: template, partial or layout)
     * @param string $file
     * @return string
     */
    public static function getTemplatePath($configurationView, $type = 'template', $file = ''): string
    {
        /**
         * @todo Refactor and move to guidelines
         */
        $path = '';
        $rootPath = $type . 'RootPath';
        $rootPaths = $type . 'RootPaths';

        // Are there path's' available?!
        if (isset($configurationView[$rootPaths]) && !empty($configurationView[$rootPaths])) {
            // first, sort by key
            krsort($configurationView[$rootPaths]);
            $keys = array_keys($configurationView[$rootPaths]);
            // find the first Template in array
            for ($i = 0; $i < count($keys); $i++) {
                // Template really exists?
                $path = GeneralUtility::getFileAbsFileName($configurationView[$rootPaths][$keys[$i]]);
                if (file_exists($path . $file)) {
                    //var_dump($keys[$i], $path, 'exist');
                    break;
                }
                //var_dump($keys[$i], $path, 'not exist');

                //echo "<br>";
            }
        } else {
            if (isset($configurationView[$rootPath])) {
                //var_dump($configuration, $type, $file);
                $path = GeneralUtility::getFileAbsFileName($configurationView[$rootPath]);
                if (file_exists($path . $file)) {
                    //throw new \Exception('Tools::getTemplatePath -> path file exist');
                    // path found, everything's alright!
                } else {
                    throw new \Exception('Tools::getTemplatePath (without s) -> path file not exist');
                }
            }
        }
        if ($path === '') {
            throw new \Exception('Tools::getTemplatePath not found (' . serialize($configurationView) . ', ' . $type . ', ' . $file . ')');
        }
        return $path;
    }

    /**
     * Converts an array to string
     * @param array<mixed> $array
     * @return string
     */
    public static function arrayToString(array $array): string
    {
        ob_start();
        var_dump($array);
        return (string)ob_get_clean();
    }

    /**
     * Convert HTML to plain text, but keep line breaks and lists
     * @param string $html
     * @return mixed|string
     */
    public static function convertHtmlToPlainText($html='')
    {
        $html = str_replace('<li>', '<li>- ', $html);
        $html = str_replace('</ul>', '</ul>' . PHP_EOL, $html);
        $html = str_replace('</li>', '</li>' . PHP_EOL, $html);
        $html = html_entity_decode($html);
        $html = strip_tags($html);
        return $html;
    }
}
