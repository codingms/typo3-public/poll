<?php

namespace CodingMs\Poll\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Send mail utility
 */
class SendMailUtility
{
    /**
     * Collect error messages, while trying to send an emails
     * @var array<int, string>
     */
    protected static $errors = [];

    /**
     * SendMail
     *
     * $emailData example:
     * $emailData['toEmail']     = "typo3@coding.ms";
     * $emailData['toName']      = "Thomas Deuling";
     * $emailData['fromEmail']   = "bestellung@riederverlag.de";
     * $emailData['fromName']    = "Bestellung auf www.riederverlag.de";
     * $emailData['subject']     = "Bestellung";
     * $emailData['message']     = "Hier der Nachrichten-Text";
     *
     * @param array<string, mixed> $emailData Data for the email
     * @param array<mixed> $attachments Attachments for the mail
     * @return bool Successfully sent!?
     * @throws \Exception
     */
    public static function send(array $emailData = [], array $attachments = [])
    {
        self::$errors = [];
        $paramsFailed = false;
        if (!isset($emailData['fromEmail']) || empty($emailData['fromEmail'])) {
            self::$errors[] = 'fromEmail failed';
            $paramsFailed = true;
        }
        if (!isset($emailData['fromName']) || empty($emailData['fromName'])) {
            self::$errors[] = 'fromName failed';
            $paramsFailed = true;
        }
        if (!isset($emailData['toEmail']) || empty($emailData['toEmail'])) {
            self::$errors[] = 'toEmail failed';
            $paramsFailed = true;
        }
        if (!isset($emailData['toName']) || empty($emailData['toName'])) {
            self::$errors[] = 'toName failed';
            $paramsFailed = true;
        }
        if (!isset($emailData['subject']) || empty($emailData['subject'])) {
            self::$errors[] = 'subject failed';
            $paramsFailed = true;
        }
        if (!isset($emailData['message']) || empty($emailData['message'])) {
            self::$errors[] = 'message failed';
            $paramsFailed = true;
        }
        // When all parameters are valid, try to send mail
        if (!$paramsFailed) {
            /** @var MailMessage $mail */
            $mail = GeneralUtility::makeInstance(MailMessage::class);
            $mail->setFrom([$emailData['fromEmail'] => $emailData['fromName']]);
            $mail->setTo([$emailData['toEmail'] => $emailData['toName']]);
            $mail->setSubject($emailData['subject']);
            $mail->setBody($emailData['message']);

            /**
             * @todo refactor bcc and cc
             */

            // Possible attachments here
            foreach ($attachments as $attachment) {
                if ($attachment['type'] === 'dynamic') {
                    /* @phpstan-ignore-next-line */
                    $mail->attach(\Swift_Attachment::newInstance(
                        $attachment['body'],
                        $attachment['fileName'],
                        $attachment['contentType']
                    ));
                } else {
                    throw new \Exception('static attachment not available');
                }
            }
            if ($mail->send()) {
                $success = true;
            } else {
                $success = false;
                self::$errors[] = 'Send mail failed on ' . $emailData['toEmail'] . ' recipient!';
            }
        } else {
            $success = false;
        }
        $emailData['success'] = $success;
        return $success;
    }

    /**
     * Returns the errors of the last sending mail
     * @return array<int, string>
     */
    public static function getErrors()
    {
        return self::$errors;
    }

    /**
     * Render email content
     *
     * @param string $templatePath Path of the Templates within the typo3conf/ext directory
     * @param string $templateName Name of the template
     * @param array<string, mixed> $emailData Data for email content
     * @param array<string, mixed> $settings Extension settings
     * @return array<string, mixed>
     */
    public static function renderEmail(
        $templatePath = 'poll/Resources/Private/Templates/',
        $templateName = 'default',
        array $emailData = [],
        array $settings = []
    ) {
        /** @var \TYPO3\CMS\Fluid\View\StandaloneView $emailView */
        $emailView = GeneralUtility::makeInstance(StandaloneView::class);
        // Get template path
        $templateRootPath = GeneralUtility::getFileAbsFileName($templatePath);
        $templatePathAndFilename = $templateRootPath . 'Email/' . $templateName . '.html';
        // In case of template file exists
        if (file_exists($templatePathAndFilename)) {
            $emailView->setTemplatePathAndFilename($templatePathAndFilename);
            $emailView->assign('settings', $settings);
            $emailView->assign('data', $emailData);
            // Render subject
            $emailView->assign('part', 'subject');
            $emailData['subject'] = $emailView->render();
            // Render message
            $emailView->assign('part', 'message');
            $emailData['message'] = $emailView->render();
        } else {
            $emailData['subject'] = 'file not found: ' . $templatePathAndFilename;
            $emailData['message'] = 'file not found: ' . $templatePathAndFilename;
        }
        return $emailData;
    }
}
