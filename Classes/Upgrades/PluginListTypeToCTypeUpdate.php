<?php

declare(strict_types=1);

namespace CodingMs\Poll\Upgrades;

use CodingMs\AdditionalTca\Upgrades\AbstractListTypeToCTypeV12V13;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('pollPluginListTypeToCTypeUpdate')]
final class PluginListTypeToCTypeUpdate extends AbstractListTypeToCTypeV12V13
{
    protected function getListTypeToCTypeMapping(): array
    {
        return [
            'poll_poll' => 'poll_poll',
            'poll_pollteaser' => 'poll_pollteaser',
            'poll_pollresult' => 'poll_pollresult',
            'poll_polllist' => 'poll_polllist',
        ];
    }

    public function getTitle(): string
    {
        return 'PluginListTypeToCTypeUpdate Upgrade Wizard';
    }

    public function getDescription(): string
    {
        return 'EXT:poll: Switches ListTypes to CTypes for TYPO3 V12 and V13';
    }
}
