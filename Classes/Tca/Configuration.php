<?php

declare(strict_types=1);

namespace CodingMs\Poll\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Configuration presets for TCA fields.
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @version 1.2.0
 */
class Configuration extends \CodingMs\AdditionalTca\Tca\Configuration
{
    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label
     * @param array<mixed> $options
     * @return array<mixed>
     */
    public static function get($type, $required = false, $readonly = false, $label = '', array $options=[]): array
    {
        switch ($type) {
            case 'pollMode':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'size' => 1,
                    'items' => [
                        ['label' => 'Public participation', 'value' => 'public'],
                        ['label' => 'FrontendUser login required', 'value' => 'frontendUser'],
                    ],
                ];
                $config = self::insertRequired($config, true);
                break;
            case 'pollQuestion':
                $config = [
                    'type' => 'inline',
                    'foreign_sortby' => 'sorting',
                    'foreign_table' => 'tx_poll_domain_model_pollquestion',
                    'foreign_field' => 'poll',
                    'maxitems' => 9999,
                    'appearance' => [
                        'collapse' => 0,
                        'levelLinksPosition' => 'top',
                        'showSynchronizationLink' => 1,
                        'showPossibleLocalizationRecords' => 1,
                        'showAllLocalizationLink' => 1,
                        'useSortable' => 1,
                        'enabledControls' => [
                            'info' => true,
                            'new' => true,
                            'dragdrop' => true,
                            'sort' => true,
                            'hide' => false,
                            'delete' => true
                        ],
                    ],
                ];
                break;
            case 'singleQuestionLayout':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'size' => 1,
                    'items' => [
                        ['label' => 'Layout (normal)', 'value' => 'Default'],
                        ['label' => 'Single: Layout (inline mit Skala)', 'value' => 'InlineScaled'],
                        ['label' => 'Single: SelectBox', 'value' => 'SelectBox']
                    ],
                ];
                $config = self::insertRequired($config, true);
                break;
            case 'singleWithOptionalUserAnswerLayout':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'size' => 1,
                    'items' => [
                        ['label' => 'Layout (normal)', 'value' => 'Default'],
                    ],
                ];
                $config = self::insertRequired($config, true);
                break;
            case 'multipleQuestionLayout':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'size' => 1,
                    'items' => [
                        ['label' => 'Layout (normal)', 'value' => 'Default']
                    ],
                ];
                $config = self::insertRequired($config, true);
                break;
            case 'multipleWithOptionalUserAnswerLayout':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'size' => 1,
                    'items' => [
                        ['label' => 'Layout (normal)', 'value' => 'Default']
                    ],
                ];
                $config = self::insertRequired($config, true);
                break;
            case 'singleUserAnswerLayout':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'size' => 1,
                    'items' => [
                        ['label' => 'Layout (normal)', 'value' => 'Default'],
                        ['label' => 'SingleUserAnswer: Layout (mehrzeilig)', 'value' => 'Multiline'],
                    ],
                ];
                $config = self::insertRequired($config, true);
                break;
            case 'questionType':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'size' => 1,
                    'default' => 'Single',
                    'items' => [
                        ['label' => 'Single - Uses only radio-buttons, only a single answer possible', 'value' => 'Single'],
                        ['label' => 'Multiple - Uses only checkboxes, multiple answers possible', 'value' => 'Multiple']
                    ],
                ];
                $config = self::insertRequired($config, true);
                break;
            default:
                $config = parent::get($type, $required, $readonly, $label);
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }
}
