<?php

namespace CodingMs\Poll\Domain\Model;

use CodingMs\Modules\Domain\Model\FrontendUser;
/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Annotation as Extbase;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PollTicket extends AbstractEntity
{
    protected ?FrontendUser $frontendUser = null;

    /**
     * @var Poll
     * @Extbase\ORM\Lazy
     */
    protected $poll;

    protected bool $isFinished = false;

    /**
     * @var ObjectStorage<PollQuestion>
     */
    protected $pollQuestion;

    /**
     * @var ObjectStorage<PollTicketAnswer>
     */
    protected $pollTicketAnswer;

    public function __construct()
    {
        $this->initStorageObjects();
    }

    protected function initStorageObjects(): void
    {
        $this->pollQuestion = new ObjectStorage();
        $this->pollTicketAnswer = new ObjectStorage();
    }

    /**
     * @return ?FrontendUser
     */
    public function getFrontendUser(): ?FrontendUser
    {
        return $this->frontendUser;
    }

    /**
     * @param FrontendUser $frontendUser
     */
    public function setFrontendUser(FrontendUser $frontendUser): void
    {
        $this->frontendUser = $frontendUser;
    }

    /**
     * @return bool
     */
    public function getIsFinished(): bool
    {
        return $this->isFinished;
    }

    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->isFinished;
    }

    /**
     * @param bool $isFinished
     */
    public function setIsFinished(bool $isFinished): void
    {
        $this->isFinished = $isFinished;
    }

    /**
     * @return Poll
     */
    public function getPoll(): Poll
    {
        if ($this->poll instanceof LazyLoadingProxy) {
            $this->poll->_loadRealInstance();
        }
        return $this->poll;
    }

    /**
     * @param Poll $poll
     */
    public function setPoll(Poll $poll): void
    {
        $this->poll = $poll;
    }

    /**
     * @param PollQuestion $pollQuestion
     */
    public function addPollQuestion(PollQuestion $pollQuestion): void
    {
        $this->pollQuestion->attach($pollQuestion);
    }

    /**
     * @param PollQuestion $pollQuestionToRemove
     */
    public function removePollQuestion(PollQuestion $pollQuestionToRemove): void
    {
        $this->pollQuestion->detach($pollQuestionToRemove);
    }

    /**
     * @return ObjectStorage
     */
    public function getPollQuestion(): ObjectStorage
    {
        return $this->pollQuestion;
    }

    /**
     * @param ObjectStorage $pollQuestion
     */
    public function setPollQuestion(ObjectStorage $pollQuestion): void
    {
        $this->pollQuestion = $pollQuestion;
    }

    /**
     * @param PollTicketAnswer $pollTicketAnswer
     */
    public function addPollTicketAnswer(PollTicketAnswer $pollTicketAnswer): void
    {
        $this->pollTicketAnswer->attach($pollTicketAnswer);
    }

    /**
     * @param PollTicketAnswer $pollTicketAnswerToRemove
     */
    public function removePollTicketAnswer(PollTicketAnswer $pollTicketAnswerToRemove): void
    {
        $this->pollTicketAnswer->detach($pollTicketAnswerToRemove);
    }

    /**
     * @return ObjectStorage
     */
    public function getPollTicketAnswer(): ObjectStorage
    {
        return $this->pollTicketAnswer;
    }

    /**
     * @param ObjectStorage $pollTicketAnswer
     */
    public function setPollTicketAnswer(ObjectStorage $pollTicketAnswer): void
    {
        $this->pollTicketAnswer = $pollTicketAnswer;
    }
}
