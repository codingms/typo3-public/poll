<?php

namespace CodingMs\Poll\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation as Extbase;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Poll extends AbstractEntity
{
    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @Extbase\ORM\Lazy
     */
    protected $image;

    /**
     * @var string
     */
    protected $mode;

    /**
     * @var bool
     */
    protected $useCookieProtection;

    /**
     * @var bool
     */
    protected $useCaptchaProtection;

    /**
     * @var bool
     */
    protected $withoutPollTicket;

    /**
     * @var bool
     */
    protected $dividersToTabs = false;

    /**
     * @var bool
     */
    protected $editable;

    /**
     * @var bool
     */
    protected $sendEmailOnFinish;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestion>
     */
    protected $pollQuestion;

    public function __construct()
    {
        $this->initStorageObjects();
    }

    protected function initStorageObjects(): void
    {
        $this->pollQuestion = new ObjectStorage();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string $description
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return ?\TYPO3\CMS\Core\Resource\FileReference
     */
    public function getImage(): ?\TYPO3\CMS\Core\Resource\FileReference
    {
        if (!is_object($this->image)) {
            return null;
        }
        if ($this->image instanceof LazyLoadingProxy) {
            $this->image->_loadRealInstance();
        }

        return $this->image->getOriginalResource();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @noinspection PhpUnused
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * @return string $mode
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     * @noinspection PhpUnused
     */
    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    /**
     * @return bool $useCookieProtection
     */
    public function getUseCookieProtection(): bool
    {
        return $this->useCookieProtection;
    }

    /**
     * @param bool $useCookieProtection
     * @noinspection PhpUnused
     */
    public function setUseCookieProtection($useCookieProtection): void
    {
        $this->useCookieProtection = $useCookieProtection;
    }

    /**
     * @return bool $useCaptchaProtection
     */
    public function getUseCaptchaProtection(): bool
    {
        return $this->useCaptchaProtection;
    }

    /**
     * @param bool $useCaptchaProtection
     * @noinspection PhpUnused
     */
    public function setUseCaptchaProtection($useCaptchaProtection): void
    {
        $this->useCaptchaProtection = $useCaptchaProtection;
    }

    /**
     * @return bool $withoutPollTicket
     * @noinspection PhpUnused
     */
    public function getWithoutPollTicket(): bool
    {
        return $this->withoutPollTicket;
    }

    /**
     * @return bool $withoutPollTicket
     */
    public function isWithoutPollTicket(): bool
    {
        return $this->withoutPollTicket;
    }

    /**
     * @param bool $withoutPollTicket
     * @noinspection PhpUnused
     */
    public function setWithoutPollTicket($withoutPollTicket): void
    {
        $this->withoutPollTicket = $withoutPollTicket;
    }

    /**
     * @return bool $dividersToTabs
     * @noinspection PhpUnused
     */
    public function getDividersToTabs(): bool
    {
        return $this->dividersToTabs;
    }

    /**
     * @return bool $dividersToTabs
     */
    public function isDividersToTabs(): bool
    {
        return $this->dividersToTabs;
    }

    /**
     * @param bool $dividersToTabs
     * @noinspection PhpUnused
     */
    public function setDividersToTabs($dividersToTabs): void
    {
        $this->dividersToTabs = $dividersToTabs;
    }

    /**
     * @return bool $editable
     * @noinspection PhpUnused
     */
    public function getEditable(): bool
    {
        return $this->editable;
    }

    /**
     * @return bool $editable
     */
    public function isEditable(): bool
    {
        return $this->editable;
    }

    /**
     * @param bool $editable
     * @noinspection PhpUnused
     */
    public function setEditable($editable): void
    {
        $this->editable = $editable;
    }

    /**
     * @return bool $sendEmailOnFinish
     * @noinspection PhpUnused
     */
    public function getSendEmailOnFinish(): bool
    {
        return $this->sendEmailOnFinish;
    }

    /**
     * @return bool $sendEmailOnFinish
     */
    public function isSendEmailOnFinish(): bool
    {
        return $this->sendEmailOnFinish;
    }

    /**
     * @param bool $sendEmailOnFinish
     * @noinspection PhpUnused
     */
    public function setSendEmailOnFinish($sendEmailOnFinish): void
    {
        $this->sendEmailOnFinish = $sendEmailOnFinish;
    }

    /**
     * @param PollQuestion $pollQuestion
     * @noinspection PhpUnused
     */
    public function addPollQuestion(PollQuestion $pollQuestion): void
    {
        $this->pollQuestion->attach($pollQuestion);
    }

    /**
     * @param \CodingMs\Poll\Domain\Model\PollQuestion $pollQuestionToRemove The PollQuestion to be removed
     * @noinspection PhpUnused
     */
    public function removePollQuestion(PollQuestion $pollQuestionToRemove): void
    {
        $this->pollQuestion->detach($pollQuestionToRemove);
    }

    /**
     * @return ObjectStorage<PollQuestion> $pollQuestion
     */
    public function getPollQuestion(): ObjectStorage
    {
        return $this->pollQuestion;
    }

    /**
     * @param ObjectStorage<PollQuestion> $pollQuestion
     * @noinspection PhpUnused
     */
    public function setPollQuestion(ObjectStorage $pollQuestion): void
    {
        $this->pollQuestion = $pollQuestion;
    }

    /**
     * @return array<int|string, mixed> $resultArray
     * @throws Exception
     */
    public function getPollResultArray(): array
    {
        $resultArray = [];
        //
        // Build array structure for result data
        // ..and calculate answer count
        $questions = $this->getPollQuestion();
        if (count($questions) > 0) {
            /** @var PollQuestion $pollQuestion */
            foreach ($questions as $pollQuestion) {
                // Questions
                $pollQuestionPosition = $pollQuestion->getUid();
                $resultArray[$pollQuestionPosition]['pollQuestion'] = $pollQuestion;
                $resultArray[$pollQuestionPosition]['answerCount'] = 0;
                // Answers
                $answers = $pollQuestion->getQuestionAnswer();
                if (!empty($answers)) {
                    /** @var PollQuestionAnswer $pollAnswer */
                    foreach ($answers as $pollAnswer) {
                        $pollAnswerPosition = $pollAnswer->getUid();
                        $pollTicketInline = '(SELECT uid FROM tx_poll_domain_model_pollticket WHERE poll = ' . $this->getUid() . ' AND deleted = 0)';
                        $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswer'] = $pollAnswer;
                        if (!$this->isWithoutPollTicket()) {
                            /** @var ConnectionPool $connectionPool */
                            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
                            /** @var QueryBuilder $queryBuilder */
                            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_poll_domain_model_pollticketanswer');
                            $queryBuilder->count('uid')
                                ->from('tx_poll_domain_model_pollticketanswer')
                                ->where(
                                    $queryBuilder->expr()->in('poll_ticket', $pollTicketInline)
                                )
                                ->andWhere(
                                    $queryBuilder->expr()->eq('poll_question_answer', $pollAnswer->getUid())
                                );

                            /**
                             * @todo collect custom answers
                             */
                            $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerCount'] = $queryBuilder->executeQuery()->fetchOne();
                            $resultArray[$pollQuestionPosition]['answerCount'] += $queryBuilder->executeQuery()->fetchOne();
                        } else {
                            $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerCount'] = $pollAnswer->getAnswerCount();
                            $resultArray[$pollQuestionPosition]['answerCount'] += $pollAnswer->getAnswerCount();
                        }
                    }
                }
            }
        }
        //
        // Calculate percentages
        if (!empty($resultArray)) {
            foreach ($resultArray as $pollQuestionPosition => $pollQuestion) {
                if ($resultArray[$pollQuestionPosition]['answerCount'] > 0) {
                    $answerAmount = 0;
                    /* @phpstan-ignore-next-line */
                    foreach ($resultArray[$pollQuestionPosition]['result'] as $pollAnswerPosition => $pollAnswerCount) {
                        $oneAnswerPercent = 100 / $resultArray[$pollQuestionPosition]['answerCount'];
                        /* @phpstan-ignore-next-line */
                        $answerPercent = $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerCount'] * $oneAnswerPercent;
                        $answerPercent = (float)number_format((float)$answerPercent, 1);
                        /* @phpstan-ignore-next-line */
                        if (!isset($resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerPercent'])) {
                            $resultArray[$pollQuestionPosition]['result'][$pollAnswerPosition]['pollAnswerPercent'] = $answerPercent;
                            $answerAmount += $answerPercent;
                        }
                    }
                }
            }
        }
        return $resultArray;
    }
}
