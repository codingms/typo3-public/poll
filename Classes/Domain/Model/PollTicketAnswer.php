<?php

namespace CodingMs\Poll\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation as Extbase;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PollTicketAnswer extends AbstractEntity
{
    protected string $userAnswer = '';

    /**
     * @var PollQuestion
     * @Extbase\ORM\Lazy
     */
    protected $pollQuestion;

    protected ?PollQuestionAnswer $pollQuestionAnswer = null;

    /**
     * @return string $userAnswer
     */
    public function getUserAnswer(): string
    {
        return $this->userAnswer;
    }

    /**
     * @param string $userAnswer
     */
    public function setUserAnswer(string $userAnswer): void
    {
        $this->userAnswer = $userAnswer;
    }

    /**
     * @return PollQuestion $pollQuestion
     */
    public function getPollQuestion(): PollQuestion
    {
        //        if ($this->pollQuestion === null) {
        //            echo "";
        //        }
        if ($this->pollQuestion instanceof LazyLoadingProxy) {
            $this->pollQuestion->_loadRealInstance();
        }
        if ($this->pollQuestion === null) {
            return $this->getPollQuestionTemp();
        }
        return $this->pollQuestion;
    }

    /**
     * @param PollQuestion $pollQuestion
     */
    public function setPollQuestion(PollQuestion $pollQuestion): void
    {
        $this->pollQuestion = $pollQuestion;
    }

    /**
     * @return \CodingMs\Poll\Domain\Model\PollQuestionAnswer $pollQuestionAnswer
     */
    public function getPollQuestionAnswer()
    {
        if ($this->pollQuestionAnswer === null) {
            return $this->getPollQuestionAnswerTemp();
        }
        if ($this->pollQuestionAnswer instanceof LazyLoadingProxy) {
            $this->pollQuestionAnswer->_loadRealInstance();
        }
        return $this->pollQuestionAnswer;
    }

    /**
     * @param PollQuestionAnswer $pollQuestionAnswer
     */
    public function setPollQuestionAnswer(PollQuestionAnswer $pollQuestionAnswer): void
    {
        $this->pollQuestionAnswer = $pollQuestionAnswer;
    }

    /**
     * Returns a temp object of a deleted poll-question-answer for export
     *
     * @return PollQuestionAnswer
     * @throws \Doctrine\DBAL\Exception
     */
    protected function getPollQuestionAnswerTemp(): PollQuestionAnswer
    {
        $pollTicketRecord = $this->getRawData();
        $pollQuestionAnswerRecord = $this->getPollQuestionAnswerRaw($pollTicketRecord['poll_question_answer']);
        //
        // Create a temp object of deleted record for export
        $pollQuestionAnswerTemp = new PollQuestionAnswer();
        $pollQuestionAnswerTemp->setAnswer($pollQuestionAnswerRecord['answer'] . ' [deleted]');
        $pollQuestionAnswerTemp->setSorting(1);
        return $pollQuestionAnswerTemp;
    }

    /**
     * Get the deleted poll-question-answer
     *
     * @return array<mixed>
     * @throws \Doctrine\DBAL\Exception
     */
    protected function getPollQuestionAnswerRaw(int $uid): array
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_poll_domain_model_pollquestionanswer');
        $queryBuilder->getRestrictions()->removeAll();
        $queryBuilder->select('*')
            ->from('tx_poll_domain_model_pollquestionanswer')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            );
        return $queryBuilder->executeQuery()->fetchAssociative();
    }

    /**
     * Returns a temp object of a deleted poll-question for export
     *
     * @return PollQuestion
     * @throws \Doctrine\DBAL\Exception
     */
    protected function getPollQuestionTemp(): PollQuestion
    {
        $pollTicketRecord = $this->getRawData();
        $pollQuestionRecord = $this->getPollQuestionRaw($pollTicketRecord['poll_question']);
        //
        // Create a temp object of deleted record for export
        $pollQuestionTemp = new PollQuestion();
        $pollQuestionTemp->setQuestion($pollQuestionRecord['question'] . ' [deleted]');
        $pollQuestionTemp->setSorting(1);
        return $pollQuestionTemp;
    }

    /**
     * Get the deleted poll-question-
     *
     * @return array<mixed>
     * @throws \Doctrine\DBAL\Exception
     */
    protected function getPollQuestionRaw(int $uid): array
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_poll_domain_model_pollquestion');
        $queryBuilder->getRestrictions()->removeAll();
        $queryBuilder->select('*')
            ->from('tx_poll_domain_model_pollquestion')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            );
        return $queryBuilder->executeQuery()->fetchAssociative();
    }

    /**
     * @return array<mixed>
     * @throws \Doctrine\DBAL\Exception
     */
    protected function getRawData(): array
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        //
        // First, fetch raw database record of this object
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_poll_domain_model_pollticketanswer');
        $queryBuilder->select('*')
            ->from('tx_poll_domain_model_pollticketanswer')
            ->where(
                $queryBuilder->expr()->eq('uid', $this->getUid())
            );
        return $queryBuilder->executeQuery()->fetchAssociative();
    }
}
