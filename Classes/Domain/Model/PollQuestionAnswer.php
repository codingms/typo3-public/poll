<?php

namespace CodingMs\Poll\Domain\Model;

use TYPO3\CMS\Extbase\Annotation as Extbase;
/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PollQuestionAnswer extends AbstractEntity
{
    /**
     * @var int
     */
    protected $sorting;

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $answer;

    /**
     * @var int
     */
    protected $answerCount;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @Extbase\ORM\Lazy
     */
    protected $image;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var bool
     */
    protected $useUserAnswer;

    /**
     * @var string
     */
    protected $userAnswerPlaceholder;

    /**
     * @var string
     */
    protected $tempUserAnswer;

    /**
     * @return int
     */
    public function getSorting(): int
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting(int $sorting): void
    {
        $this->sorting = $sorting;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }

    /**
     * @return int
     */
    public function getAnswerCount(): int
    {
        return $this->answerCount;
    }

    /**
     * @param int $answerCount
     */
    public function setAnswerCount(int $answerCount): void
    {
        $this->answerCount = $answerCount;
    }

    public function incrementAnswerCount(): void
    {
        $this->answerCount++;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return ?\TYPO3\CMS\Core\Resource\FileReference
     */
    public function getImage(): ?\TYPO3\CMS\Core\Resource\FileReference
    {
        if (!is_object($this->image)) {
            return null;
        }
        if ($this->image instanceof LazyLoadingProxy) {
            $this->image->_loadRealInstance();
        }

        return $this->image->getOriginalResource();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image): void
    {
        $this->image = $image;
    }

    /**
     * @return bool
     */
    public function getUseUserAnswer(): bool
    {
        return $this->useUserAnswer;
    }

    /**
     * @param bool $useUserAnswer
     */
    public function setUseUserAnswer(bool $useUserAnswer): void
    {
        $this->useUserAnswer = $useUserAnswer;
    }

    /**
     * @return string
     */
    public function getUserAnswerPlaceholder(): string
    {
        return $this->userAnswerPlaceholder;
    }

    /**
     * @param string $userAnswerPlaceholder
     */
    public function setUserAnswerPlaceholder(string $userAnswerPlaceholder): void
    {
        $this->userAnswerPlaceholder = $userAnswerPlaceholder;
    }

    /**
     * @return string
     */
    public function getTempUserAnswer(): string
    {
        return $this->tempUserAnswer;
    }

    /**
     * @param string $tempUserAnswer
     */
    public function setTempUserAnswer(string $tempUserAnswer): void
    {
        $this->tempUserAnswer = $tempUserAnswer;
    }
}
