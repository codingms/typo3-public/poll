<?php

namespace CodingMs\Poll\Domain\Model;

use TYPO3\CMS\Extbase\Annotation as Extbase;
/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PollQuestion extends AbstractEntity
{
    /**
     * @var int
     */
    protected $sorting;

    /**
     * @var string
     */
    protected $questionHeadline;

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $question;

    /**
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $questionType;

    /**
     * @var string
     */
    protected $questionLayout;

    /**
     * @var string
     */
    protected $additionalCssClass;

    /**
     * @var bool
     */
    protected $showHrBefore;

    /**
     * @var bool
     */
    protected $optional;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Poll\Domain\Model\PollQuestionAnswer>
     */
    protected $questionAnswer;

    /**
     * @var string
     */
    protected $questionAnswerDefault;

    public function __construct()
    {
        $this->initStorageObjects();
    }

    protected function initStorageObjects(): void
    {
        $this->questionAnswer = new ObjectStorage();
    }

    /**
     * @return int $sorting
     */
    public function getSorting(): int
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting(int $sorting): void
    {
        $this->sorting = $sorting;
    }

    /**
     * @return string
     */
    public function getQuestionHeadline(): string
    {
        return $this->questionHeadline;
    }

    /**
     * @param string $questionHeadline
     */
    public function setQuestionHeadline(string $questionHeadline): void
    {
        $this->questionHeadline = $questionHeadline;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return string $questionType
     */
    public function getQuestionType()
    {
        return $this->questionType;
    }

    /**
     * @param string $questionType
     */
    public function setQuestionType(string $questionType): void
    {
        $this->questionType = $questionType;
    }

    /**
     * @return string
     */
    public function getQuestionLayout(): string
    {
        return $this->questionLayout;
    }

    /**
     * @param string $questionLayout
     */
    public function setQuestionLayout(string $questionLayout): void
    {
        $this->questionLayout = $questionLayout;
    }

    /**
     * @return string
     */
    public function getAdditionalCssClass(): string
    {
        return $this->additionalCssClass;
    }

    /**
     * @param string $additionalCssClass
     */
    public function setAdditionalCssClass(string $additionalCssClass): void
    {
        $this->additionalCssClass = $additionalCssClass;
    }

    /**
     * @return bool
     */
    public function getShowHrBefore(): bool
    {
        return $this->showHrBefore;
    }

    /**
     * @return bool
     */
    public function isShowHrBefore(): bool
    {
        return $this->showHrBefore;
    }

    /**
     * @param bool $showHrBefore
     */
    public function setShowHrBefore(bool $showHrBefore): void
    {
        $this->showHrBefore = $showHrBefore;
    }

    /**
     * @return bool
     */
    public function getOptional(): bool
    {
        return $this->optional;
    }

    /**
     * @return bool
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * @param bool $optional
     */
    public function setOptional(bool $optional): void
    {
        $this->optional = $optional;
    }

    /**
     * @return string
     */
    public function getQuestionAnswerDefault(): string
    {
        return $this->questionAnswerDefault;
    }

    /**
     * @param string $questionAnswerDefault
     */
    public function setQuestionAnswerDefault(string $questionAnswerDefault): void
    {
        $this->questionAnswerDefault = $questionAnswerDefault;
    }

    /**
     * @param PollQuestionAnswer $questionAnswer
     */
    public function addQuestionAnswer(PollQuestionAnswer $questionAnswer): void
    {
        $this->questionAnswer->attach($questionAnswer);
    }

    /**
     * @param PollQuestionAnswer $questionAnswerToRemove
     */
    public function removeQuestionAnswer(PollQuestionAnswer $questionAnswerToRemove): void
    {
        $this->questionAnswer->detach($questionAnswerToRemove);
    }

    /**
     * @return ObjectStorage
     */
    public function getQuestionAnswer(): ObjectStorage
    {
        return $this->questionAnswer;
    }

    /**
     * @param ObjectStorage $questionAnswer
     */
    public function setQuestionAnswer(ObjectStorage $questionAnswer): void
    {
        $this->questionAnswer = $questionAnswer;
    }
}
