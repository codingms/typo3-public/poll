<?php

namespace CodingMs\Poll\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Domain\Model\FrontendUser;
use CodingMs\Poll\Domain\Model\Poll;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Poll ticket repository
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class PollTicketRepository extends Repository
{
    /**
     * Find the a poll ticket for a frontend user
     *
     * @param Poll $poll Poll-Object
     * @param FrontendUser $frontendUser FrontendUser-Object
     * @return object|null
     */
    public function findOneByPollAndFrontendUser(Poll $poll, FrontendUser $frontendUser)
    {
        $query = $this->createQuery();
        $constraints = [];
        $constraints[] = $query->equals('poll', $poll);
        $constraints[] = $query->equals('frontendUser', $frontendUser);
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        $query->setLimit(1);
        return $query->execute()->getFirst();
    }

    /**
     * Find the last poll ticket from a frontend user
     *
     * @param FrontendUser $frontendUser FrontendUser-Object
     * @return object|null
     */
    public function findLastOneFromFrontendUser(FrontendUser $frontendUser)
    {
        $orderings = [
            'uid' => QueryInterface::ORDER_DESCENDING
        ];
        $query = $this->createQuery();
        $query->setOrderings($orderings);
        $constraints = [];
        $constraints[] = $query->equals('frontendUser', $frontendUser);
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        $query->setLimit(1);
        return $query->execute()->getFirst();
    }

    /**
     * Find the Poll
     *
     * @param Poll $poll
     * @param bool $respectStoragePage
     * @return QueryResultInterface|object[]
     */
    public function findByPoll($poll, $respectStoragePage = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage($respectStoragePage);
        $constraints = [];
        $constraints[] = $query->equals('poll', $poll);
        $constraints[] = $query->equals('isFinished', true);
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        return $query->execute();
    }

    /**
     * Find the Poll
     *
     * @param Poll $poll
     * @param array<int, mixed> $startEndTime
     * @param bool $respectStoragePage
     * @return QueryResultInterface|object[]
     * @throws InvalidQueryException
     */
    public function findByPollAndTimeFrame($poll, $startEndTime, $respectStoragePage = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage($respectStoragePage);
        $constraints = [];
        $constraints[] = $query->equals('poll', $poll);
        $constraints[] = $query->equals('isFinished', true);
        if (!empty($startEndTime[0])) {
            $constraints[] = $query->greaterThanOrEqual('tstamp', (int)$startEndTime[0]);
        }
        if (!empty($startEndTime[1])) {
            $constraints[] = $query->lessThanOrEqual('tstamp', (int)$startEndTime[1]);
        }
        if (count($constraints) > 1) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        } else {
            $query->matching($constraints[0]);
        }
        return $query->execute();
    }

    /**
     * @param array<string, mixed> $filter
     * @param bool $count
     * @return QueryResultInterface|object[]|int
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllForBackendList(array $filter = [], $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $query->matching(
            $query->equals('poll', $filter['poll']['selected'])
        );
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] !== '') {
                if ($filter['sortingOrder'] === 'asc') {
                    $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] === 'desc') {
                        $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }
}
