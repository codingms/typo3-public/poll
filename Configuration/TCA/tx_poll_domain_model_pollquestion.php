<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_pollquestion';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'question_headline',
        'label_alt' => 'show_hr_before, question',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'type' => 'question_type',
        'hideTable' => 1,
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-poll-poll'
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'types' => [
        '1' => [
            'showitem' => '
            --palette--;;question_headline_question_type,
            question,
            optional,

        --div--;' . $lll . '.tab_answers,
            question_answer_default, question_answer,

        --div--;' . $lll . '.tab_layout,
            --palette--;;layout'
        ],
        'Single' => [
            'showitem' => '
            --palette--;;question_headline_question_type,
            question,
            optional,

        --div--;' . $lll . '.tab_answers,
            question_answer_default, question_answer,

        --div--;' . $lll . '.tab_layout,
            --palette--;;layout',
            'columnsOverrides' => [
                'question_layout' => [
                    'config' => [
                        'items' => \CodingMs\Poll\Tca\Configuration::get('singleQuestionLayout')['items']
                    ]
                ]
            ]
        ],
        'Multiple' => [
            'showitem' => '
            --palette--;;question_headline_question_type,
            question,
            optional,

        --div--;' . $lll . '.tab_answers,
            question_answer_default, question_answer,

        --div--;' . $lll . '.tab_layout,
            --palette--;;layout',
            'columnsOverrides' => [
                'question_layout' => [
                    'config' => [
                        'items' => \CodingMs\Poll\Tca\Configuration::get('multipleQuestionLayout')['items']
                    ]
                ]
            ]
        ],
    ],
    'palettes' => [
        'question_headline_question_type' => ['showitem' => 'question_headline, question_type, --linebreak--, question_type_information'],
        'layout' => ['showitem' => 'question_layout, additional_css_class, show_hr_before'],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\Poll\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\Configuration::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\Configuration::full('endtime'),
        'question_headline' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:poll/Resources/Private/Language/locallang_db.xlf:tx_poll_domain_model_pollquestion.question_headline',
            'description' => 'LLL:EXT:poll/Resources/Private/Language/locallang_db.xlf:tx_poll_domain_model_pollquestion.question_headline_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('string'),
        ],
        'question' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:poll/Resources/Private/Language/locallang_db.xlf:tx_poll_domain_model_pollquestion.question',
            'config' => \CodingMs\Poll\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'sorting' => [
            'exclude' => 0,
            'label' => $lll . '.sorting',
            'config' => \CodingMs\Poll\Tca\Configuration::get('int'),
        ],
        'question_type' => [
            'exclude' => 0,
            'label' => $lll . '.question_type',
            'description' => $lll . '.question_type_description',
            'onChange' => 'reload',
            'config' => \CodingMs\Poll\Tca\Configuration::get('questionType')
        ],
        'question_type_information' => [
            'exclude' => 0,
            'label' => '',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => $lll . '.question_type_information',
            ]),
        ],
        'question_layout' => [
            'exclude' => 0,
            'label' => $lll . '.question_layout',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'required' => true,
                'items' => [],
            ],
        ],
        'additional_css_class' => [
            'exclude' => 0,
            'label' => $lll . '.additional_css_class',
            'config' => \CodingMs\Poll\Tca\Configuration::get('string'),
        ],
        'show_hr_before' => [
            'exclude' => 1,
            'label' => $lll . '.show_hr_before',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox', false, false, $lll . '.show_hr_before_label'),
        ],
        'optional' => [
            'exclude' => 1,
            'label' => $lll . '.optional',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'question_answer_default' => [
            'exclude' => 0,
            'label' => $lll . '.question_answer_default',
            'displayCond' => [
                'OR' => [
                    'FIELD:question_type:=:Single',
                    'FIELD:question_type:=:SingleWithOptionalUserAnswer',
                    'FIELD:question_type:=:SingleUserAnswer',
                ],
            ],
            'config' => \CodingMs\Poll\Tca\Configuration::get('string'),
        ],
        'question_answer' => [
            'exclude' => 0,
            'label' => $lll . '.question_answer',
            'description' => $lll . '.question_answer_description',
            'config' => [
                'type' => 'inline',
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tx_poll_domain_model_pollquestionanswer',
                'foreign_field' => 'poll_question',
                'minitems' => 1,
                'maxitems' => 9999,
                'appearance' => [
                    'collapse' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'useSortable' => 1,

                    /* Loeschen, Erstellen, etc Buttons ausblenden */
                    'enabledControls' => [
                        'info' => true,
                        'new' => true,
                        'dragdrop' => true,
                        'sort' => true,
                        'hide' => false,
                        'delete' => true
                    ],
                ],
            ],
        ],
        'poll' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];

return $return;
