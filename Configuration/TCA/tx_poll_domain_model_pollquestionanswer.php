<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_pollquestionanswer';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'answer',
        'label_alt' => 'answer_count',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'hideTable' => 1,
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-poll-poll'
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'types' => [
        '1' => [
            'showitem' => '
            --palette--;;answer,
            description,
        --div--;' . $lll . '.tab_image,
            image,
        --div--;' . $lll . '.tab_user_answer,
        '],
    ],
    'palettes' => [
        'answer' => ['showitem' => 'answer, answer_count'],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\Poll\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\Configuration::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\Configuration::full('endtime'),
        'answer' => [
            'exclude' => 0,
            'label' => $lll . '.answer',
            'description' => $lll . '.answer_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('string', true),
        ],
        'answer_count' => [
            'exclude' => 0,
            'label' => $lll . '.answer_count',
            'description' => $lll . '.answer_count_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('int', false, true),
        ],
        'sorting' => [
            'exclude' => 0,
            'label' => $lll . '.sorting',
            'description' => $lll . '.sorting_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('int'),
        ],
        'image' => [
            'exclude' => 0,
            'label' => $lll . '.image',
            'config' => \CodingMs\Poll\Tca\Configuration::get('imageSingleAltTitle'),
        ],
        'description' => [
            'exclude' => 0,
            'label' => $lll . '.description',
            'description' => $lll . '.description_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'use_user_answer' => [
            'exclude' => 0,
            'label' => $lll . '.use_user_answer',
            'onChange' => 'reload',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'user_answer_placeholder' => [
            'exclude' => 0,
            'displayCond' => 'FIELD:use_user_answer:>:0',
            'label' => $lll . '.user_answer_placeholder',
            'config' => \CodingMs\Poll\Tca\Configuration::get('string'),
        ],
        'poll_question' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];

return $return;
