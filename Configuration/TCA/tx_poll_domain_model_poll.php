<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_poll';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'sortby' => 'sorting',
        'crdate' => 'crdate',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-poll-poll'
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'types' => [
        '1' => [
            'showitem' => '
            information,
            title,
            description,
            mode,
            slug,
            --palette--;' . $lll . '.palette_protection;protection,
            without_poll_ticket,
            dividers_to_tabs,
            editable,
        --div--;' . $lll . '.tab_questions,
            --palette--;;poll_question,
        --div--;' . $lll . '.tab_image,
            image,
        --div--;' . $lll . '.tab_language,
            sys_language_uid;;;;1-1-1,
            l10n_parent,
            l10n_diffsource,
        --div--;' . $lll . '.tab_access,
            hidden;;1,
            starttime,
            endtime'
        ],
    ],
    'palettes' => [
        'protection' => ['showitem' => 'use_cookie_protection'],
        'poll_question' => ['showitem' => 'poll_question_information, --linebreak--, poll_question'],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\Poll\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\Configuration::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\Configuration::full('endtime'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'description' => $lll . '.title_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('string', true),
        ],
        'description' => [
            'exclude' => 0,
            'label' => $lll . '.description',
            'description' => $lll . '.description_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'image' => [
            'exclude' => 0,
            'label' => $lll . '.image',
            'config' => \CodingMs\Poll\Tca\Configuration::get('imageSingleAltTitle'),
        ],
        'mode' => [
            'exclude' => 1,
            'label' => $lll . '.mode',
            'onChange' => 'reload',
            'config' => \CodingMs\Poll\Tca\Configuration::get('pollMode'),
        ],
        'use_cookie_protection' => [
            'displayCond' => 'FIELD:mode:=:public',
            'exclude' => 1,
            'label' => $lll . '.use_cookie_protection',
            'description' => $lll . '.use_cookie_protection_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'use_captcha_protection' => [
            'displayCond' => 'FIELD:mode:=:public',
            'exclude' => 1,
            'label' => $lll . '.use_captcha_protection',
            'description' => $lll . '.use_captcha_protection_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'without_poll_ticket' => [
            'exclude' => 1,
            'label' => $lll . '.without_poll_ticket',
            'description' => $lll . '.without_poll_ticket_description',
            'onChange' => 'reload',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'dividers_to_tabs' => [
            'exclude' => 1,
            'label' => $lll . '.dividers_to_tabs',
            'description' => $lll . '.dividers_to_tabs_description',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'editable' => [
            'displayCond' => [
                'AND' => [
                    'FIELD:mode:=:frontendUser',
                    'FIELD:without_poll_ticket:REQ:FALSE',
                ],
            ],
            'exclude' => 1,
            'label' => $lll . '.editable',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'send_email_on_finish' => [
            'exclude' => 1,
            'label' => $lll . '.send_email_on_finish',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'poll_question_information' => [
            'exclude' => 0,
            'label' => '',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => $lll . '.poll_question_information',
                'display' => 'warning',
            ]),
        ],
        'poll_question' => [
            'exclude' => 0,
            'label' => $lll . '.poll_question',
            'config' => \CodingMs\Poll\Tca\Configuration::get('pollQuestion'),
        ],
        'slug' => [
            'exclude' => 1,
            'label' => $lll . '.slug',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('slug', false, false, '', [
                'field' => 'title'
            ]),
        ],
    ],
];

return $return;
