<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_pollticket';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'poll',
        'label_alt' => 'frontend_user',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-poll-pollticket'
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'types' => [
        '1' => ['showitem' => 'information, poll, frontend_user, poll_ticket_answer, is_finished'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\Poll\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\Configuration::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\Configuration::full('endtime'),
        'frontend_user' => [
            'exclude' => 0,
            'label' => $lll . '.frontend_user',
            'description' => $lll . '.frontend_user_description',
            'displayCond' => 'FIELD:frontend_user:>:0',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'fe_users',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 0,
                'readOnly' => 1,
                'default' => '0',
            ],
        ],
        'is_finished' => [
            'exclude' => 1,
            'label' => $lll . '.is_finished',
            'config' => \CodingMs\Poll\Tca\Configuration::get('checkbox'),
        ],
        'poll' => [
            'exclude' => 0,
            'label' => $lll . '.poll',
            'description' => $lll . '.poll_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_poll_domain_model_poll',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'readOnly' => 1,
            ],
        ],
        'poll_ticket_answer' => [
            'exclude' => 0,
            'label' => $lll . '.poll_ticket_answer',
            'description' => $lll . '.poll_ticket_answer_description',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_poll_domain_model_pollticketanswer',
                'foreign_field' => 'poll_ticket',
                'maxitems' => 9999,
                'appearance' => [
                    'collapse' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'enabledControls' => [
                        'info' => false,
                        'new' => false,
                        'dragdrop' => false,
                        'sort' => false,
                        'hide' => false,
                        'delete' => false
                    ],
                ],
            ],
        ],
    ],
];

return $return;
