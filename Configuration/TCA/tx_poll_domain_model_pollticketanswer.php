<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'poll';
$table = 'tx_poll_domain_model_pollticketanswer';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'poll_question',
        'label_alt' => 'poll_question_answer',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'hideTable' => 1,
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-poll-pollticket'
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'types' => [
        '1' => ['showitem' => '
            poll_question,
            user_answer,
            poll_question_answer'
        ],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\Poll\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Poll\Tca\Configuration::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\Poll\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Poll\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Poll\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Poll\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Poll\Tca\Configuration::full('endtime'),
        'user_answer' => [
            'exclude' => 0,
            'label' => $lll . '.user_answer',
            'description' => $lll . '.user_answer_description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'readOnly' => 1,
                'default' => '',
            ],
        ],
        'poll_question' => [
            'exclude' => 0,
            'label' => $lll . '.poll_question',
            'description' => $lll . '.poll_question_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_poll_domain_model_pollquestion',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'readOnly' => 1,
            ],
        ],
        'poll_question_answer' => [
            'exclude' => 0,
            'label' => $lll . '.poll_question_answer',
            'description' => $lll . '.poll_question_answer_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_poll_domain_model_pollquestionanswer',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'readOnly' => 1,
            ],
        ],
    ],
];

return $return;
