<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    'label' => 'LLL:EXT:poll/Resources/Private/Language/locallang_db.xlf:tx_poll_label.contains_polls',
    'value' => 'polls',
    'icon' => 'apps-pagetree-folder-contains-polls'
];

$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-polls'] = 'apps-pagetree-folder-contains-polls';
