<?php

defined('TYPO3') or die();
//
// Static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'poll',
    'Configuration/TypoScript',
    'Poll'
);
