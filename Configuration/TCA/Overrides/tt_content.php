<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\CodingMs\AdditionalTca\Utility\PluginUtility::add(
    'poll',
    'Poll',
    'poll'
);
\CodingMs\AdditionalTca\Utility\PluginUtility::add(
    'poll',
    'PollTeaser',
    'poll'
);
\CodingMs\AdditionalTca\Utility\PluginUtility::add(
    'poll',
    'PollResult',
    'poll'
);
\CodingMs\AdditionalTca\Utility\PluginUtility::add(
    'poll',
    'PollList',
    'poll'
);

//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    'poll',
//    'Poll',
//    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll'
//);
//
//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    'poll',
//    'PollTeaser',
//    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll_teaser'
//);
//
//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    'poll',
//    'PollResult',
//    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll_result'
//);
//
//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    'poll',
//    'PollList',
//    'LLL:EXT:poll/Resources/Private/Language/locallang.xlf:tx_poll_domain_model_poll.show_poll_list'
//);
////
//// Include flex forms
//$flexForms = ['Poll', 'PollList', 'PollTeaser', 'PollResult'];
//foreach ($flexForms as $pluginName) {
//    $pluginSignature = 'poll_' . strtolower($pluginName);
//    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
//    $flexForm = 'FILE:EXT:poll/Configuration/FlexForms/' . $pluginName . '.xml';
//    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, $flexForm);
//}
//
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_poll'] = 'recursive,select_key,pages';
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_pollteaser'] = 'recursive,select_key,pages';
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_pollresult'] = 'recursive,select_key,pages';
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['poll_polllist'] = 'recursive,select_key,pages';
//
