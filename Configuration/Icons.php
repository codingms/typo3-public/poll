<?php

declare(strict_types=1);

use TYPO3\CMS\Core\Imaging\IconProvider\SvgSpriteIconProvider;

return [
    'apps-pagetree-folder-contains-polls' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-checkbox-20.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#apps-pagetree-folder-contains-polls',
    ],
    'actions-poll-tickets' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-language-10.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#actions-poll-tickets',
    ],
    'content-plugin-poll-poll' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-checkbox-20.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#content-plugin-poll-poll',
    ],
    'content-plugin-poll-pollteaser' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-checkbox-20.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#content-plugin-poll-pollteaser',
    ],
    'content-plugin-poll-pollresult' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-checkbox-20.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#content-plugin-poll-pollresult',
    ],
    'content-plugin-poll-polllist' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-checkbox-20.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#content-plugin-poll-polllist',
    ],
    'mimetypes-x-content-poll-poll' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-help-6.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#mimetypes-x-content-poll-poll',
    ],
    'mimetypes-x-content-poll-pollticket' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:poll/Resources/Public/Icons/iconmonstr-language-10.svg',
        'sprite' => 'EXT:poll/Resources/Public/Icons/backend-sprites.svg#mimetypes-x-content-poll-pollticket',
    ],
];
