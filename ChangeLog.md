# Poll Changelog

## 2025-01-19 Release of version 5.0.x

*	[TASK] Add missing icon sprite for dark mode
*	[TASK] Add missing documentation about CSV exports
*	[TASK] Add missing JavaScript for Tab navigation in frontend



## 2025-01-19 Release of version 5.0.0

*	[TASK] Fix code style
*	[TASK] Migrate flex-form array keys
*	[TASK] Migrate registering page-tsconfig
*	[TASK] Migrate to plugins to content elements and icon registration
*	[TASK] Migrate to TYPO3 13, remove support for TYPO3 11
*	[BUGFIX] Fix deniedNewTables page typoscript for prevent creating records in list module
*	[TASK] Normalize names like poll ticket, umfrage-ticket and umfrage-zettel
*	[TASK] Migrate TypoScript imports



## 2025-01-31 Release of version 4.0.15

*	[BUGFIX] Fix deniedNewTables page typoscript for prevent creating records in list module
*	[TASK] Normalize names like poll ticket, umfrage-ticket and umfrage-zettel
*	[TASK] Migrate TypoScript imports



## 2024-03-26 Release of version 4.0.14

*	[BUGFIX] Set min items of question answers to 1 in order to avoid issues



## 2024-03-12 Release of version 4.0.13

*	[TASK] Optimize TCA descriptions and labels for better usability
*	[TASK] Optimize checkbox translation/description in divider2tabs feature in backend
*	[TASK] Clean up JavaScript code
*	[TASK] Optimize version conditions in PHP code



## 2023-11-07 Release of version 4.0.12

*	[BUGFIX] Fix PHP 7.4 support



## 2023-11-01 Release of version 4.0.11

*	[TASK] Clean up documentation
*	[BUGFIX] Fix get content object for TYPO3 11



## 2023-10-13  Release of version 4.0.10

*	[TASK] Add documentation about provided features



## 2023-10-10 Release of version 4.0.9

*	[BUGFIX] Move modules dependency from pro into base extension
*	[BUGFIX] Add missing poll object into result fluid template
*	[BUGFIX] Fix link action in list-link from result-view



## 2023-10-04 Release of version 4.0.8

*	[TASK] Extension clean up



## 2023-10-04 Release of version 4.0.7

*	[TASK] Optimize documentation articles
*	[TASK] Extend backend module and more TYPO3 12 migrations



## 2023-09-13 Release of version 4.0.6

*	[BUGFIX] Add a missing div closing tag



## 2023-09-11 Release of version 4.0.5

*	[BUGFIX] Fix Fluid template paths related to pro-templates



## 2023-09-07 Release of version 4.0.4

*	[BUGFIX] Fix PHP 7.4 issue in PollAnswer model



## 2023-09-07 Release of version 4.0.3

*	[BUGFIX] Fix PHP 7.4 issue in Poll model
*	[BUGFIX] Fix CSV export if there are deleted questions or answers contained
*	[TASK] Add a notice in backend about modifications on poll records



## 2023-08-15 Release of version 4.0.2

*	[BUGFIX] Fix frontend user model usage
*	[BUGFIX] Fix fluid form handling and smaller migration bugs



## 2023-08-14 Release of version 4.0.1

*	[BUGFIX] Fix fluid form handling and smaller migration bugs



## 2023-08-14 Release of version 4.0.0

*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10



## 2023-09-13 Release of version 3.1.2

*	[BUGFIX] Add a missing div closing tag



## 2023-04-28 Release of version 3.1.1

*	[TASK] Migrate CSH to TCA descriptions
*	[TASK] Create TypoScript constants documentation
*	[TASK] Optimize translations for backend



## 2023-04-03 Release of version 3.1.0

*	[FEATURE] Add checkboxes to disallow passing polls by parameter in plugins
*	[FEATURE] Allow to display poll result immediately with a plugin
*	[TASK] Optimize features in documentation configuration
*	[BUGFIX] Fix Configuration usage in TCA definitions
*	[TASK] Migrate TYPO3 PHP constants
*	[BUGFIX] Fix missing icons in TCA definitions
*	[TASK] Optimize documentation for human readable urls
*	[TASK] Migrate ext_tables and ext_localconf



## 2022-12-08 Release of version 3.0.3

*	[BUGFIX] Add information field in database in order to avoid SQL issues


## 2022-10-25 Release of version 3.0.2

*	[TASK] Optimize and extend documentation
*	[BUGFIX] Fix TCA type for answer_count from string to int



## 2022-06-30 Release of version 3.0.1

*	[BUGFIX] Fix poll answer validation for single and single-with-optional-user-answer
*	[TASK] Assume all query parameters in poll result redirect, so that a usage as nested record is possible (for example within a news record)



## 2022-06-04 Release of version 3.0.0

*	[FEATURE] Add an additional possibility to select a result page in poll flex form
*	[TASK] Optimize error handling when try to access restricted polls
*	[TASK] Migration for TYPO3 11 and drop support for TYPO3 8 and 9
*	[TASK] Add dependency for additional_tca in order to reduce the maintenance overhead
*	[TASK] Preparation for TYPO3 11



## 2022-04-29 Release of version 2.2.5

*	[TASK] Move extension icon into public folder
*	[BUGFIX] Fix jQuery usage
*	[BUGFIX] Fix documentation configuration
*	[TASK] Add documentations configuration



## 2021-09-07 Release of version 2.2.4

*	[TASK] Extend cookie lifetime for poll participation's
*	[TASK] Fix typo in a label



## 2021-07-27 Release of version 2.2.3

*	[BUGFIX] Fix fetching poll result - ignore all deleted records



## 2021-07-21 Release of version 2.2.2

*	[BUGFIX] Update TCA configuration for starttime and endtime



## 2021-07-09 Release of version 2.2.1

*	[BUGFIX] Add missing database definition for sorting



## 2021-07-04 Release of version 2.2.0

*	[TASK] Refactoring of the saving logic
*	[FEATURE] Add optional-checkbox in all question types
*	[TASK] Add pre-selected answer for SingleWithOptionalUserAnswer
*	[TASK] Optimize backend title for questions
*	[TASK] Add additional documentation for plugins



## 2021-06-30 Release of version 2.1.1

*	[TASK] Add missing EN translations.



## 2021-06-29 Release of version 2.1.0

*	[!!!] This version changes the database structure.
*	[FEATURE] Make poll records sortable and add sorting option to list plugin (manual sorting, crdate or title)



## 2021-06-25 Release of version 2.0.9

*	[TASK] Enable default answer text for SingleUserAnswer



## 2021-05-21 Release of version 2.0.8

*	[BUGFIX] Fix links in documentation
*	[TASK] TCA clean up - remove unused sorting configuration



## 2021-05-15 Release of version 2.0.7

*	[TASK] Add extension key in composer.json
*	[TASK] TS config clean up
*	[TASK] Add documentation notice about user answers



## 2020-10-08 Release of version 2.0.6

*	[BUGFIX] Frontend user check from all PIDs
*	[TASK] Plugin translations
*	[BUGFIX] Frontend user check



## 2020-09-04 Release of version 2.0.5

*	[TASK] Added description text to answer description rte
*	[TASK] Optimize extension description for better visibility in TER



## 2020-08-27 Release of version 2.0.4

*	[BUGFIX] Remove unimplemented combinations of question type and layout



## 2020-07-20 Release of version 2.0.3

*	[BUGFIX] Fix message translations in Poll controller
*	[TASK] Add links to bottom of readme files



## 2020-07-06 Release of version 2.0.2

*	[TASK] Add documentation translations
*	[TASK] Add required namespace for Extbase annotation in domain model



## 2020-06-24 Release of version 2.0.1

*	[TASK] Update documentation
*	[TASK] Update extension description



## 2020-06-24 Release of version 2.0.0

*	[BUGFIX] Fix a bug where answers weren't saved correctly
*	[TASK] Add language label translations
*	[TASK] Poll result refactoring
*	[TASK] Migrate field anonym to without_poll_ticket
*	[TASK] Poll teaser refactoring
*	[TASK] Clean up TCA and Models
*	[TASK] Clean up constants.typoscript file and add labels
*	[TASK] Add Pagetree icon and new content element wizards see #1 and #2
*	[TASK] Refactoring of ext_tables.php
*	[TASK] Preparations for version 2.0.0
*	[TASK] Remove Debug service
*	[TASK] Remove inject annotations and migration lazy annotations



## 2019-10-13  Release of version 1.2.0

*	[TASK] Add Gitlab-CI configuration.



## 2017-11-14  Release of version 1.1.0

*	[BUGFIX] Fixing a poll relation issue. Removing poll_ticket from poll.
*	[FEATURE] Adding frontend admin reset poll feature
*	[TASK] FlashMessage 8.7 Migration
*	[BUGFIX] Fixing a poll saving bug - reset the save service after each iteration
*	[TASK] Add a finish flag in poll ticket that indicates that a poll ticket has been finished
